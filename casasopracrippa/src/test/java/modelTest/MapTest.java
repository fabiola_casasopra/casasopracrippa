package modelTest;

import static org.junit.Assert.*;

import java.io.IOException;

import mapsmodel.*;

import org.junit.Before;
import org.junit.Test;

import sectormodel.*;


public class MapTest {
	MapFactory mapFactory;
	GalileiMap glm;
	GalvaniMap gvm;
	FermiMap frm;
	TestMap tsm;
	AlienSector as;
	HumanSector hs;
	DangerousSector ds;
	SecureSector ss;
	
	@Before
	public void setUp() throws IOException {
		mapFactory = new MapFactory();
		glm = GalileiMap.getInstance();
		gvm = GalvaniMap.getInstance();
		frm = FermiMap.getInstance();
		tsm = new TestMap();
		as = new AlienSector('L', 9);
		hs = new HumanSector('L', 10);
		ds = new DangerousSector('L', 3);
		ss = new SecureSector('L', 5);
	}
	
	@Test
	public void testMapFactory() throws IOException {
		assertEquals(null, mapFactory.getMap("no map"));
		assertEquals(glm.toString(), mapFactory.getMap("galilei").toString());
		assertEquals(gvm.toString(), mapFactory.getMap("galvani").toString());
		assertEquals(frm.toString(), mapFactory.getMap("fermi").toString());
	}
	
	@Test
	public void testFindAlienSector(){
		assertEquals(as.toString(), frm.findAlienSector().toString() );
		assertEquals(null, tsm.findAlienSector());
	}
	
	@Test
	public void testFindHumanSector(){
		assertEquals(hs.toString(), frm.findHumanSector().toString() );
		assertEquals(null, tsm.findHumanSector());
	}
	
	@Test
	public void testCheckSector(){
		assertFalse(frm.checkSector('L', 3));
		assertTrue(frm.checkSector('L', 5));
	}

}
