package modelTest;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import mapsmodel.*;

import org.junit.Before;
import org.junit.Test;

import deckcardmodel.*;
import playermodel.*;
import sectormodel.*;
import servercontroller.SocketHandler;

public class PlayerTest {

	Player player;
	AlienPlayer ap;
	HumanPlayer hp;
	Sector sector, sectorvic1, sectorvic2, finalSector;
	AlienSector as;
	HumanSector hs;
	CharacterCardDeck chDeck;
	SectorCardDeck secDeck;
	ArrayList<Card> nCard;
	ArrayList<Sector> nSector, mSector;
	FermiMap map;
	PlayerList players;
	List<SocketHandler> sockethandlers;
	Socket sck;

	@Before
	public void setUp() throws IOException {
		sector = new Sector('D', 5);
		sectorvic1 = new Sector('D', 6);
		sectorvic2 = new Sector('E', 7);
		as = new AlienSector('L', 9);
		hs = new HumanSector('L', 10);
		player = new Player(sector);
		sck = new Socket();
		ap = new AlienPlayer(as);
		hp = new HumanPlayer(hs);
		nSector = new ArrayList<Sector>();
		nSector.add(sectorvic1);
		nSector.add(sectorvic2);
		mSector = new ArrayList<Sector>();
		chDeck = new CharacterCardDeck(2);
		secDeck = new SectorCardDeck();
		map = FermiMap.getInstance();
		sockethandlers = new LinkedList<SocketHandler>();
		for(int i=0; i<chDeck.size(); i++)
			sockethandlers.add(new SocketHandler(new Socket()));
		players = new PlayerList(sockethandlers, chDeck, as, hs);
	}

	@Test
	public void test() {
		// Test for Constructor
		assertTrue(sector.equals(player.getSector()));
		assertEquals("Player", player.toString());
		assertTrue(player.alive());
		assertEquals(sector, player.revealToAll());
		player.endPlay();
		assertTrue(player.isHisTurn() == false);
		
		// Test for Constructor
		player = new AlienPlayer(sector);
		assertTrue(sector.equals(player.getSector()));
		assertEquals("AlienPlayer", player.toString());
				
		// Test for Constructor
		player = new HumanPlayer(sector);
		assertTrue(sector.equals(player.getSector()));
		assertEquals("HumanPlayer", player.toString());
		
		// Test for socket
		player.setSocket(sck);
		assertEquals(sck, player.getSocket());
	}

	@Test
	public void testValidMove() {
		finalSector = new Sector('E', 7);
		assertTrue(player.validMove(finalSector, nSector));
		finalSector = new Sector('A', 1);
		assertFalse(player.validMove(finalSector, nSector));
		finalSector = new Sector('E', 1);
		assertFalse(player.validMove(finalSector, nSector));
	}
	
	@Test
	public void testPickCard() {
		chDeck = new CharacterCardDeck(2);
		nCard = new ArrayList<Card>();
		nCard.add(new Silence());
		Card card = player.pickCard(nCard);
		assertTrue(card instanceof Silence);
		card = chDeck.get(0);
		Card pickedCard = player.pickCard(chDeck);
		assertEquals(card, pickedCard);
		card = secDeck.get(0);
		pickedCard = player.pickCard(secDeck);
		assertEquals(card, pickedCard);	
	}
	
	@Test
	public void testClone() {
		Player play = player.clone();
		assertEquals(play, player);
	}
	
	@Test
	public void testHisTurn() {
		player.setHisTurn();
		assertTrue(player.isHisTurn());
		player.endTurn();
		assertFalse(player.isHisTurn());
	}
	
	@Test
	public void testConnected() {
		player.setConnected(true);
		assertTrue(player.isConnected());
		player.setConnected(false);
		assertFalse(player.isConnected());
	}
	
	@Test
	public void testAfk(){
		player.setAfk();
		assertTrue(player.isAfk());
	}
	
	@Test
	public void testWherePlayerCanMove() throws IOException{
		mSector = player.wherePlayerCanMove( map );
		assertTrue( mSector == null );
	}
	
	@Test
	public void testSetSector() throws IOException {
		Sector sec = ap.setMove( map, 'L', 8);
		ap.setSector(sec);
		assertEquals(sec, ap.getSector());
		sec = ap.setMove( map, '~', 8);
		assertEquals(sec, null);
		sec = ap.setMove( map, 'L', -7);
		assertEquals(sec, null);
		sec = ap.setMove( map, 'z', 8);
		assertEquals(sec, null);
		sec = ap.setMove( map, 'L', -1);
		assertEquals(sec, null);
		ap.setSector( new SecureSector('K', 2));
		sec = ap.setMove( map, 'K', 3);
		ap.setSector(sec);
		assertEquals(sec, ap.getSector());
		sec = hp.setMove(map, 'L', 11);
		hp.setSector(sec);
		assertEquals(sec, hp.getSector());
	}
	
	@Test
	public void testAttack(){
		Sector sec = new DangerousSector('L', 4);
		ap.setId(2);
		ap.setSector(sec);
		players.get(0).setSector(sec);
		players.get(1).setSector(new DangerousSector('L', 3));
		ap.attack(players);
		assertFalse(players.get(0).alive());
		assertTrue(players.get(1).alive());
		players.get(1).setSector(new DangerousSector('M', 4));
		ap.attack(players);
		assertFalse(players.get(0).alive());
		assertTrue(players.get(1).alive());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testIllegalArgumentExceptionAp() {
		// Test for Constructor Exception
		sector = null;
		player = new AlienPlayer(sector);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testIllegalArgumentExceptionHp() {
		// Test for Constructor Exception
		sector = null;
		player = new HumanPlayer(sector);
	}
}
