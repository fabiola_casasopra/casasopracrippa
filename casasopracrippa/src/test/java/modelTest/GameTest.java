package modelTest;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

import gamemodel.Game;
import mapsmodel.FermiMap;

import org.junit.Before;
import org.junit.Test;

import deckcardmodel.*;
import playermodel.HumanPlayer;
import sectormodel.DangerousSector;
import sectormodel.Sector;
import servercontroller.SocketHandler;

public class GameTest {
	
	Game game;
	List<SocketHandler> sockethandlers;
	Sector sec;
	Card card;

	@Before
	public void setUp() {
		sockethandlers = new LinkedList<SocketHandler>();
		sockethandlers.add(new SocketHandler(new Socket()));
		sockethandlers.add(new SocketHandler(new Socket()));
		game = new Game(sockethandlers, "Fermi");
	}
	
	@Test
	public void test() {
		assertTrue(game.getMap() instanceof FermiMap);
		assertTrue(game.getNumPlayer() == 2);
		assertTrue(game.getCountTurn() == 0);
		game.turnCounter();
		assertTrue(game.getCountTurn() == 1);
	}
	
	@Test
	public void testMove() {
		try {
			for(int i=0; i<game.getNumPlayer(); i++) {
				game.turnCounter();
				sec = game.move('D', 5);
				assertEquals(null, sec);
				if(game.getPlayer() instanceof HumanPlayer) {
					sec = game.move('L', 11);
					assertEquals('L', sec.getX());
					assertEquals(11, sec.getY());
				}
				else {
					sec = game.move('L', 8);
					assertEquals('L', sec.getX());
					assertEquals(8, sec.getY());
				}
			}
		} catch (IOException e) {}
	}
	
	@Test
	public void testPickCard() {
		for(int i=0; i<game.getNumPlayer()*39; i++) {
			game.turnCounter();
			card = game.pickSectorCard();
			assertTrue(card instanceof Silence || card instanceof NoiseInAnySector
					|| card instanceof NoiseInYourSector);
		}
	}
	
	@Test
	public void testEndGame() {
		assertFalse(game.endGame());
		for(int i=0; i<game.getNumPlayer()*39; i++) {
			game.turnCounter();
		}
		assertTrue(game.endGame());
	}
	
	@Test
	public void tesAttack() {
		sec = new DangerousSector('L', 3);
		game.turnCounter();
		game.getPlayer().setSector(sec);
		game.turnCounter();
		game.getPlayer().setSector(sec);
		for(int i=0; i<game.getNumPlayer(); i++) {
			game.turnCounter();
			game.attack();
		}
		for(int i=0; i<game.getNumPlayer(); i++) {
			if(game.getPlayer() instanceof HumanPlayer)
				assertFalse(game.getPlayer().alive());
		}
		assertTrue(game.endGame());
	}
	
	@Test
	public void testCheckSector() {
		assertTrue(game.checkSector('L', 3));
		assertFalse(game.checkSector('L', 5));
	}
}