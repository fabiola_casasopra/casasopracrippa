package modelTest;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import playermodel.Player;
import sectormodel.Sector;
import deckcardmodel.*;

public class CardTest {
	ArrayList<Card> secCard, secCardFalse;
	ArrayList<Card> charCardOdd, charCardEven, charCardOne, charCardHuman;
	Deck secDeck, charDeckOdd, charDeckEven, charDeckOne, charDeckHuman;
	int numCharCardOdd, numCharCardEven, numSecCard, numCharCard = 1;
	Player player;
	Sector sector;
	
	@Before
	public void setUp() {
		numSecCard = 25;
		secCard = new ArrayList<Card>();
		for(int i=0; i<5; i++){
			secCard.add(new Silence());
			secCard.add(new NoiseInAnySector());
			secCard.add(new NoiseInYourSector());
			secCard.add(new NoiseInAnySector());
			secCard.add(new NoiseInYourSector());
		}
		secCardFalse = new ArrayList<Card>();
		for(int i=0; i<4; i++){
			secCardFalse.add(new Silence());
			secCardFalse.add(new NoiseInAnySector());
			secCardFalse.add(new NoiseInYourSector());
			secCardFalse.add(new NoiseInAnySector());
			secCardFalse.add(new NoiseInYourSector());
		}
		secCardFalse.add(new Silence());
		secCardFalse.add(new HumanCard());
		secCardFalse.add(new NoiseInYourSector());
		secCardFalse.add(new NoiseInAnySector());
		secCardFalse.add(new NoiseInYourSector());
		numCharCardOdd = 5;
		charCardOdd = new ArrayList<Card>();
		charCardOdd.add(new AlienCard());
		charCardOdd.add(new AlienCard());
		charCardOdd.add(new AlienCard());
		charCardOdd.add(new HumanCard());
		charCardOdd.add(new HumanCard());
		numCharCardEven = 6;
		charCardEven = new ArrayList<Card>();
		charCardEven.add(new AlienCard());
		charCardEven.add(new AlienCard());
		charCardEven.add(new AlienCard());
		charCardEven.add(new HumanCard());
		charCardEven.add(new HumanCard());
		charCardEven.add(new HumanCard());
		charCardOne = new ArrayList<Card>();
		charCardOne.add(new AlienCard());
		charCardHuman = new ArrayList<Card>();
		charCardHuman.add(new HumanCard());
		charCardHuman.add(new NoiseInYourSector());
		charCardHuman.add(new HumanCard());
		charCardHuman.add(new AlienCard());
		charCardHuman.add(new NoiseInYourSector());
		charCardHuman.add(new AlienCard());
		sector = new Sector('D', 5);
		player = new Player(sector);
	}

	@Test
	public void testDeck() {
		
		secDeck = new SectorCardDeck();
		charDeckOdd = new CharacterCardDeck( numCharCardOdd );
		charDeckEven = new CharacterCardDeck( numCharCardEven );
		charDeckOne = new CharacterCardDeck( numCharCard );
		charDeckHuman =  new CharacterCardDeck( numCharCardEven );

		// Test for buidDeck
		assertTrue(charDeckOdd.similarTo(charCardOdd));
		assertTrue(charDeckEven.similarTo(charCardEven));
		assertFalse(charDeckOdd.similarTo(charCardEven));
		assertTrue(secDeck.similarTo(secCard));

		// Test for shuffleDeck
		assertFalse(charDeckOdd.equalsTo(charCardOdd));
		assertFalse(charDeckEven.equalsTo(charCardEven));
		assertFalse(secDeck.equalsTo(secCard));
		
		// Test for equalsTo
		assertTrue(charDeckOne.equalsTo(charCardOne));
		
		// Test for similarTo
		assertFalse(charDeckEven.similarTo(charCardHuman));
		assertFalse(secDeck.similarTo(secCardFalse));
	}
	
	@Test
	public void testToString() {
		Card card;
		card = new AlienCard();
		assertEquals("AlienCard", card.toString());
		card = new HumanCard();
		assertEquals("HumanCard", card.toString());
		card = new Silence();
		assertEquals("SilenceCard", card.toString());
		card = new NoiseInAnySector();
		assertEquals("NoiseInAnySectorCard", card.toString());
		card = new NoiseInYourSector();
		assertEquals("NoiseInYourSectorCard", card.toString());
	}
}
