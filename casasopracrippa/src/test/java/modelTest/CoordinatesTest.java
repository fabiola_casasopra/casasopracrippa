package modelTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import sectormodel.Coordinates;

public class CoordinatesTest {

	Coordinates coord;

	@Before
	public void setUp() {
		coord = new Coordinates('D', 5);
	}

	@Test
	public void test() {
		// Test for Constructor
		assertEquals(5, coord.getY());
		assertEquals('D', coord.getX());

		// Test for getters and setters
		coord.setX('A');
		assertEquals('A', coord.getX());
		coord.setY('2');
		assertEquals('2', coord.getY());
	}
}
