package modelTest;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import playermodel.HumanPlayer;
import playermodel.Player;
import sectormodel.*;

public class SectorTest {

	Sector sector;
	Player player;
	ArrayList<Sector> nSector, mSector;

	@Before
	public void setUp() {
		sector = new Sector('D', 5);
		player = new Player(sector);
		nSector = new ArrayList<Sector>();
		mSector = new ArrayList<Sector>();
		nSector.add(new Sector('D', 4));
		nSector.add(new Sector('D', 5));
		mSector.add(new Sector('D', 4));
		mSector.add(new Sector('E', 6));
	}

	@Test
	public void test() throws CloneNotSupportedException {
		// Test for Constructor
		assertEquals(5, sector.getY());
		assertEquals('D', sector.getX());
		assertEquals("Sector", sector.toString());
		assertTrue(sector.isThere(nSector));
		assertFalse(sector.isThere(mSector));
	}
	

	@Test
	public void testAlienSector() {
		sector = new AlienSector('D', 5);
		// Test for Constructor
		assertEquals(5, sector.getY());
		assertEquals('D', sector.getX());
		assertEquals("AlienSector", sector.toString());
	}

	@Test
	public void testHumanSector() {
		sector = new HumanSector('D', 5);
		// Test for Constructor
		assertEquals(5, sector.getY());
		assertEquals('D', sector.getX());
		assertEquals("HumanSector", sector.toString());
	}

	@Test
	public void testDangerousSector() {
		sector = new DangerousSector('D', 5);
		// Test for Constructor
		assertEquals(5, sector.getY());
		assertEquals('D', sector.getX());
		assertEquals("DangerousSector", sector.toString());
	}

	@Test
	public void testSecureSector() {
		sector = new SecureSector('D', 5);
		// Test for Constructor
		assertEquals(5, sector.getY());
		assertEquals('D', sector.getX());
		assertEquals("SecureSector", sector.toString());
	}

	@Test
	public void testEscapeHatchSector() {
		sector = new EscapeHatchSector('D', 5);
		player = new HumanPlayer(sector);
		// Test for Constructor
		assertEquals(5, sector.getY());
		assertEquals('D', sector.getX());
		assertEquals("EscapeHatchSector", sector.toString());
		((EscapeHatchSector) sector).humanVictory(player);
		assertEquals(player.isVictory(), true);
	}
}
