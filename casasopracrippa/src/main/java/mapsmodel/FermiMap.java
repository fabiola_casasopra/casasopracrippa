package mapsmodel;

import java.io.IOException;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Singleton Class that represent the Fermi Map
 *
 */
public class FermiMap extends GameMap {
	
	private static FermiMap instance;
	
	/**
	 * The Fermi Map
	 */
	private static final int mapFermi[][] = {
		{5,5,5,5,5,5,5,5,5,4,5,5,5,4,5,5,5,5,5,5,5,5,5},
		{5,5,5,5,5,5,5,5,5,5,1,5,0,5,5,5,5,5,5,5,5,5,5},
		{5,5,5,5,5,5,5,5,5,0,1,0,1,0,5,5,5,5,5,5,5,5,5},
		{5,5,5,5,5,5,5,5,5,1,0,0,0,1,5,5,5,5,5,5,5,5,5},
		{5,5,5,5,5,5,5,5,5,4,5,1,5,4,5,5,5,5,5,5,5,5,5},
		{5,5,5,5,5,5,5,5,1,5,5,1,5,5,1,5,5,5,5,5,5,5,5},
		{5,5,5,5,5,5,5,5,0,1,5,1,5,0,1,5,5,5,5,5,5,5,5},
		{5,5,5,5,5,5,5,5,5,1,1,1,1,1,5,5,5,5,5,5,5,5,5},
		{5,5,5,5,5,5,5,5,0,5,5,3,5,5,1,5,5,5,5,5,5,5,5},
		{5,5,5,5,5,5,5,1,1,1,5,2,5,1,0,1,5,5,5,5,5,5,5},
		{5,5,5,5,5,5,5,1,5,1,1,1,1,1,5,0,5,5,5,5,5,5,5},
		{5,5,5,5,5,5,5,5,0,1,5,1,5,0,1,5,5,5,5,5,5,5,5},
		{5,5,5,5,5,5,5,5,5,5,0,5,1,5,5,5,5,5,5,5,5,5,5},
		{5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5},
	};
	
	/**
	 * Method that create the Fermi Map
	 * @throws IOException
	 */
	private FermiMap() throws IOException {
		super();
		createMap();
	}
	
	/**
	 * Singleton method which instantates the Fermi Map if it is null
	 * @return the Fermi Map instance
	 */
	public static FermiMap getInstance(){
		if(instance == null)
			try {
				instance = new FermiMap();
			} catch (IOException e) {
				// do something
			}
		return instance;
	}

	/**
	 * Method that create the Map assigning all the sectors
	 */
	@Override
	protected void createMap() throws IOException {
		rankSector(mapFermi);
	}
	
	/**
	 * Method that print the name of the Map
	 */
	@Override
	public String toString(){
		return "FermiMap";
	}
}