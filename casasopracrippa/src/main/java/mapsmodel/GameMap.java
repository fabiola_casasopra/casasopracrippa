package mapsmodel;

import java.io.IOException;
import java.util.ArrayList;

import sectormodel.AlienSector;
import sectormodel.DangerousSector;
import sectormodel.EscapeHatchSector;
import sectormodel.HumanSector;
import sectormodel.Sector;
import sectormodel.SecureSector;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Abstract Class that represent the 14*23 Game Map
 */
public abstract class GameMap {

	private ArrayList<ArrayList<Sector>> totalMap;
	ArrayList<Sector> mapElements;
	protected static final int ROW = 14;
	protected static final int COL = 23;
	protected char col;
	protected int currentSector;
	protected int neighborSector1;
	protected int neighborSector2;
	protected int neighborSector3;
	protected int neighborSector4;
	protected int neighborSector5;
	protected int neighborSector6;

	/**
	 * Method that initializes all the fields of the Map
	 */
	public GameMap() {
		// create the ArrayList of MapElements
		setTotalMap(new ArrayList<ArrayList<Sector>>());
	}

	/**
	 * Method that initializes all the sectors of the map and their neighbors
	 * 
	 * @param map to create
	 * @throws IOException
	 * @throws BiffException 
	 */
	public void rankSector(int[][] map) throws IOException {
		
		// initialize all the sector in the map
		for (int i = 0; i < ROW; i++) {
			col = 'A';
			for (int j = 0; j < COL; j++) {
				currentSector = map[i][j];
				Sector type;
				int coordx;
				
				// new ArrayList for any loop, in order to add a reference to
				// different mapElements to the totalMap
				mapElements = new ArrayList<Sector>();

				if (typeSector(currentSector, i, col) != null){
					mapElements.add(typeSector(currentSector, i, col));
			
					try {
						neighborSector1 = map[i-1][j];
						type = typeSector(neighborSector1, i - 1, col);
						if ((type instanceof SecureSector)
								|| (type instanceof DangerousSector)
								|| (type instanceof EscapeHatchSector))
							mapElements.add(typeSector(neighborSector1, i - 1, col));
					} catch (ArrayIndexOutOfBoundsException e) {
						//System.out.println("Out of Map 1");
					}

					try {
						if ((j % 2) == 0)
							coordx = i - 1;
						else
							coordx = i;

						neighborSector2 = map[coordx][j +1];
						type = typeSector(neighborSector2, coordx, (char) (col + 1));
						if ((type instanceof SecureSector)
								|| (type instanceof DangerousSector)
								|| (type instanceof EscapeHatchSector))
							mapElements.add(typeSector(neighborSector2, coordx,
									(char) (col + 1)));
					} catch (ArrayIndexOutOfBoundsException e) {
						//System.out.println("Out of Map 2");
					}

					try {
						if ((j % 2) == 0)
							coordx = i;
						else
							coordx = i + 1;

						neighborSector3 = map[coordx][j+1];
						type = typeSector(neighborSector3, coordx, (char) (col + 1));
						if ((type instanceof SecureSector)
								|| (type instanceof DangerousSector)
								|| (type instanceof EscapeHatchSector))
							mapElements.add(typeSector(neighborSector3, coordx,
									(char) (col + 1)));
					} catch (ArrayIndexOutOfBoundsException e) {
						//System.out.println("Out of Map 3");
					}

					try {
						neighborSector4 = map[i+1][j];
						type = typeSector(neighborSector4, i + 1, col);
						if ((type instanceof SecureSector)
								|| (type instanceof DangerousSector)
								|| (type instanceof EscapeHatchSector))
							mapElements
									.add(typeSector(neighborSector4, i + 1, col));
					} catch (ArrayIndexOutOfBoundsException e) {
						//System.out.println("Out of Map 4");
					}

					try {
						if ((j % 2) == 0)
							coordx = i;
						else
							coordx = i + 1;

						neighborSector5 = map[coordx][j-1];
						type = typeSector(neighborSector5, coordx, (char) (col - 1));
						if ((type instanceof SecureSector)
								|| (type instanceof DangerousSector)
								|| (type instanceof EscapeHatchSector))
							mapElements.add(typeSector(neighborSector5, coordx,
									(char) (col - 1)));
					} catch (ArrayIndexOutOfBoundsException e) {
						//System.out.println("Out of Map 5");
					}

					try {
						if ((j % 2) == 0)
							coordx = i - 1;
						else
							coordx = i;

						neighborSector6 = map[coordx][j-1];
						type = typeSector(neighborSector6, coordx, (char) (col - 1));
						if ((type instanceof SecureSector)
								|| (type instanceof DangerousSector)
								|| (type instanceof EscapeHatchSector))
							mapElements.add(typeSector(neighborSector6, coordx,
									(char) (col - 1)));
						} catch (ArrayIndexOutOfBoundsException e) {
							//System.out.println("Out of Map 6");
					}
				

					// add the ArrayList of mapElements to the ArrayList of totalMap
					getTotalMap().add(mapElements);
					
				}
				
				else{
					mapElements.add( null );
					getTotalMap().add(mapElements);
				}
		
				col = (char) (col + 1);
			}
		}
	}

	/**
	 * Method that determinate the type of the current sector
	 * 
	 * @param currentSector, the current sector to create
	 * @param i, the row coordinate of the sector
	 * @param col, the column coordinate of the sector
	 * @return the type of the sector
	 */
	public Sector typeSector(int currentSector, int i, char col) {

		// 0 is DangerousSector
		if (currentSector == 0) {
			return new DangerousSector(col, i + 1);
		}
		// 1 is SecureSector
		else if (currentSector == 1) {
			return new SecureSector(col, i + 1);
		}
		// 2 is HumanSector
		else if (currentSector == 2) {
			return new HumanSector(col, i + 1);
		}
		// 3 is AlienSector
		else if (currentSector == 3) {
			return new AlienSector(col, i + 1);
		}
		// 4 is EscapeHatchSector
		else if (currentSector == 4) {
			return new EscapeHatchSector(col, i + 1);
		}
		// 5 is NULL, if sector is null do nothing
		else 
			return null;
	}

	/**
	 * Method that check if a sector is in the map && if it's a dangerous sector 
	 * @param x, the x coordinate of the sector
	 * @param y, the y coordinate of the sector
	 * @return false if the sector is in the map && it's a dangerous sector
	 */
	public boolean checkSector(char x, int y) {
		for (ArrayList<Sector> elem : getTotalMap()) {
			if (elem.get(0) != null && elem.get(0).getX() == x && elem.get(0).getY() == y
					&& elem.get(0) instanceof DangerousSector)
				return false;
		}
		return true;
	}

	/**
	 * Method that create the map
	 */
	protected abstract void createMap() throws IOException;
	
	
	
	/**
	 * Method that find which are the sectors near a specified sector 
	 * @param sector, the sector of which I want to know the near sector 
	 * @return all the near sector
	 */
	public ArrayList<Sector> nearSector( Sector sector ){
		ArrayList<Sector> nSector = new ArrayList<Sector>();
		for( ArrayList<Sector> elem : this.getTotalMap() )
			if( elem.get(0) != null && elem.get(0).getX() == sector.getX() && elem.get(0).getY() == sector.getY()){
				for( Sector sec : elem )
					if( sec.getX() != sector.getX() || sec.getY() != sector.getY() )
						nSector.add( sec );
			}
		return nSector;
	}
	
	/**
	 * Method that find which are the sectors near a specified sector
	 * @param sector, the sector of which I want to know the near sector 
	 * @return all the near sector
	 */
	public ArrayList<Sector> nearSector( Sector sector, Sector sectorV ){
		ArrayList<Sector> nSector = new ArrayList<Sector>();
		for( ArrayList<Sector> elem : this.getTotalMap() )
			if( elem.get(0) != null && elem.get(0).getX() == sector.getX() && elem.get(0).getY() == sector.getY()){
				for( Sector sec : elem )
					if( (sec.getX() != sector.getX() || sec.getY() != sector.getY()) && (sec.getX() != sectorV.getX() || sec.getY() != sectorV.getY()))
						nSector.add( sec );
			}
		return nSector;
	}
	
	/**
	 * Method that find which are the sector that can be reached with two moves from a specified sector 
	 * @param sector, the sector of which I want to know which sector I can reach
	 * @return all the sector i can reach with two moves
	 */
	public ArrayList<Sector> maximumDistanceTwoSector( Sector sector ){
		ArrayList<Sector> nSector = new ArrayList<Sector>();
		ArrayList<Sector> tSector = new ArrayList<Sector>();
		for( ArrayList<Sector> elem : this.getTotalMap() ){
			if( elem.get(0) != null && elem.get(0).getX() == sector.getX() && elem.get(0).getY() == sector.getY()){
				for( Sector sec : elem ){
					if( (sec.getX() != sector.getX() || sec.getY() != sector.getY()) && !sec.isThere(nSector) ){
						nSector.add( sec );
					}
					if( sec.getX() != sector.getX() || sec.getY() != sector.getY() ){
						tSector = nearSector( sec, sector );
						for( Sector sct : tSector )
							if( !sct.isThere(nSector) )
								nSector.add( sct );
					}
				}
				nSector.add( sector );
			}
		}
		return nSector;
	}
	
	/**
	 * Method that find which is the start sector for humans
	 * @return the start sector for humans
	 */
	public Sector findHumanSector(){
		for( ArrayList<Sector> elem : this.getTotalMap() )
			if( elem.get(0) instanceof HumanSector)
				return elem.get(0);
		return null;
	}
	
	/**
	 * Method that find which is the start sector for aliens
	 * @return the start sector for aliens
	 */
	public Sector findAlienSector(){
		for( ArrayList<Sector> elem : this.getTotalMap() )
			if( elem.get(0) instanceof AlienSector)
				return elem.get(0);
		return null;
	}
	
	/**
	 * @return the totalMap
	 */
	public ArrayList<ArrayList<Sector>> getTotalMap() {
		return totalMap;
	}

	/**
	 * @param totalMap the totalMap to set
	 */
	public void setTotalMap(ArrayList<ArrayList<Sector>> totalMap) {
		this.totalMap = totalMap;
	}
}
