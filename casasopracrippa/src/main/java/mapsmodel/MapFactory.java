package mapsmodel;

import java.io.IOException;

/**
 * 
 * @author Mattia Crippa & Fabioal Casasopra
 * Class that represent the Map Factory
 */
public class MapFactory {

	/**
	 * Factory method used to get object of type Map
	 * @param mapType, the name of the Map
	 * @return the instance of the Map
	 * @throws IOException
	 */
	public GameMap getMap(String mapType) throws IOException {
		if (mapType.equalsIgnoreCase("Galilei")) {
			return GalileiMap.getInstance();
		} else if (mapType.equalsIgnoreCase("Fermi")) {
			return FermiMap.getInstance();
		} else if (mapType.equalsIgnoreCase("Galvani")) {
			return GalvaniMap.getInstance();
		}
		return null;
	}
}