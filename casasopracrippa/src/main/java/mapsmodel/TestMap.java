package mapsmodel;

import java.io.IOException;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the Test Map
 *
 */
public class TestMap extends GameMap{

	/**
	 * The Test Map
	 */
	private static final int mapTest[][] = {
		{5,5,5,5,5,5,5,5,5,4,5,5,5,4,5,5,5,5,5,5,5,5,5},
		{5,5,5,5,5,5,5,5,5,5,1,5,0,5,5,5,5,5,5,5,5,5,5},
		{5,5,5,5,5,5,5,5,5,0,1,0,1,0,5,5,5,5,5,5,5,5,5},
		{5,5,5,5,5,5,5,5,5,1,0,0,0,1,5,5,5,5,5,5,5,5,5},
		{5,5,5,5,5,5,5,5,5,4,5,1,5,4,5,5,5,5,5,5,5,5,5},
		{5,5,5,5,5,5,5,5,1,5,5,1,5,5,1,5,5,5,5,5,5,5,5},
		{5,5,5,5,5,5,5,5,0,1,5,1,5,0,1,5,5,5,5,5,5,5,5},
		{5,5,5,5,5,5,5,5,5,1,1,1,1,1,5,5,5,5,5,5,5,5,5},
		{5,5,5,5,5,5,5,5,0,5,5,5,5,5,1,5,5,5,5,5,5,5,5},
		{5,5,5,5,5,5,5,1,1,1,5,5,5,1,0,1,5,5,5,5,5,5,5},
		{5,5,5,5,5,5,5,1,5,1,1,1,1,1,5,0,5,5,5,5,5,5,5},
		{5,5,5,5,5,5,5,5,0,1,5,1,5,0,1,5,5,5,5,5,5,5,5},
		{5,5,5,5,5,5,5,5,5,5,0,5,1,5,5,5,5,5,5,5,5,5,5},
		{5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5},
	};
	
	/**
	 * Method that create the Test Map
	 * @throws IOException
	 */
	public TestMap() throws IOException {
		super();
		createMap();
	}

	/**
	 * Method that create the Map assigning all the sectors
	 */
	@Override
	protected void createMap() throws IOException {
		rankSector(mapTest);
	}
	
}
