package mapsmodel;

import java.io.IOException;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Singleton Class that represent the Galilei Map
 */
public class GalileiMap extends GameMap {
	
	private static GalileiMap instance;
	
	/**
	 * The Galilei Map
	 */
	private static final int mapGalilei[][] = {
		{5,0,1,5,5,1,0,1,1,1,0,0,0,0,5,1,1,1,5,5,1,1,5},
		{0,4,0,0,1,0,0,1,0,0,1,1,1,0,0,0,0,0,0,0,0,4,0},
		{0,0,0,0,0,0,0,1,0,0,0,0,0,1,5,1,0,0,5,5,0,0,1},
		{1,0,0,5,0,0,0,0,0,0,0,1,0,0,5,1,1,1,0,5,0,0,1},
		{1,1,0,0,0,0,0,5,0,0,1,0,1,0,1,0,0,0,0,0,1,0,1},
		{1,0,0,5,0,0,0,0,5,0,0,3,0,0,0,0,1,1,0,0,0,0,1},
		{5,5,0,5,5,0,1,1,0,5,5,5,5,5,0,0,0,1,0,1,0,5,5},
		{5,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,1,0,1,0,1,5},
		{1,0,0,0,0,0,0,0,1,0,1,1,1,0,1,0,0,0,0,5,0,0,0},
		{1,1,0,1,0,1,0,5,0,0,0,0,0,0,0,0,0,5,5,5,0,0,1},
		{1,0,0,0,0,0,0,5,0,0,1,1,1,0,0,0,1,5,5,0,0,0,1},
		{1,0,0,0,1,5,1,0,5,5,0,0,0,0,0,1,0,1,0,0,1,0,1},
		{1,4,0,0,0,5,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,4,0},
		{0,0,1,1,5,5,1,1,1,1,0,1,0,1,1,0,1,5,5,1,0,0,0}
	};

	/**
	 * Method that create the Galilei Map
	 * @throws IOException
	 */
	private GalileiMap() throws IOException {
		super();
		createMap();
	}
	
	/**
	 * Singleton method which instantates the Galilei Map if it is null
	 * @return the Galilei Map instance
	 */
	public static GalileiMap getInstance(){
		if(instance == null)
			try {
				instance = new GalileiMap();
			} catch (IOException e) {
				// do something
			}
		return instance;
	}

	/**
	 * Method that create the Map assigning all the sectors
	 */
	@Override
	protected void createMap() throws IOException {
		rankSector(mapGalilei);
	}
	
	/**
	 * Method that print the name of the Map
	 */
	@Override
	public String toString(){
		return "GalileiMap";
	}
}
