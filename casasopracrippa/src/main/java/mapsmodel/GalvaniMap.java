package mapsmodel;

import java.io.IOException;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Singleton Class that represent the Galvani Map
 */
public class GalvaniMap extends GameMap {
	
	private static GalvaniMap instance;
	
	/**
	 * The Galvani Map
	 */
	private static final int mapGalvani[][] = {
		{5,5,5,0,0,4,5,5,5,0,0,0,0,0,5,4,0,0,0,0,0,1,5},
		{5,0,0,5,5,0,0,0,0,5,5,5,0,1,0,0,1,5,0,0,5,0,5},
		{0,5,5,0,0,5,5,0,0,0,1,1,5,5,1,0,0,0,5,5,0,0,0},
		{0,1,0,5,5,0,0,5,5,0,0,5,0,0,5,5,0,0,0,5,0,0,0},
		{0,5,0,5,0,5,5,1,0,5,5,0,1,5,1,0,5,1,0,5,5,5,0},
		{0,5,0,5,0,5,0,5,5,0,0,3,0,0,5,5,0,5,0,5,5,5,0},
		{0,5,0,1,0,5,0,1,0,5,5,5,5,5,0,5,0,5,0,5,5,1,0},
		{1,5,0,5,0,5,0,5,0,0,5,2,5,0,0,5,0,5,0,5,1,0,0},
		{0,5,0,5,0,5,0,0,5,5,1,0,1,5,1,0,0,5,0,1,5,5,0},
		{0,4,0,5,0,0,1,5,0,0,1,5,5,0,0,5,5,0,0,5,0,5,0},
		{0,1,0,5,5,0,0,0,5,5,0,0,0,1,5,0,0,5,5,0,0,4,0},
		{0,0,5,0,0,5,5,1,0,5,5,1,5,0,0,5,5,5,5,0,0,5,0},
		{5,5,0,5,5,5,0,0,5,5,0,0,5,5,5,5,5,0,0,5,5,0,0},
		{5,5,5,5,5,5,5,5,0,0,1,5,1,0,1,0,1,0,0,0,0,5,5},
	};

	/**
	 * Method that create the Galvani Map
	 * @throws IOException
	 */
	private GalvaniMap() throws IOException {
		super();
		createMap();
	}
	
	/**
	 * Singleton method which instantates the Galvani Map if it is null
	 * @return the Galvani Map instance
	 */
	public static GalvaniMap getInstance(){
		if(instance == null)
			try {
				instance = new GalvaniMap();
			} catch (IOException e) {
				// do something
			}
		return instance;
	}

	/**
	 * Method that create the Map assigning all the sectors
	 */
	@Override
	protected void createMap() throws IOException {
		rankSector(mapGalvani);
	}
	
	/**
	 * Method that print the name of the Map
	 */
	@Override
	public String toString(){
		return "GalvaniMap";
	}
}