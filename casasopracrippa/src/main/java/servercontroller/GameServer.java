package servercontroller;

import gamemodel.Game;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import commoncontroller.State;
import deckcardmodel.*;
import playermodel.*;
import sectormodel.*;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the GameServer which is the object that manage the Game
 */
public class GameServer implements Runnable {
	
	private List<SocketHandler> sh;
	private State status;
	private String map = null;
	private Game game;
	private Sector sec, noiseSec;
	
	/**
	 * Constructor for the GameServer
	 * @param sockethandlers, the list of sockets of the client that are connected to the server
	 */
	public GameServer(List<SocketHandler> sockethandlers) {
		sh = sockethandlers;
	}

	/**
	 * Send the message to the client
	 * @param skh
	 * @param msgToClient
	 */
	private void sendMessage(SocketHandler skh, State msgToClient, String info){
		try{
			skh.getOutSocket().println(msgToClient + "&" + info );
			skh.getOutSocket().flush();
		}catch(NullPointerException ex){
			this.status = State.DISCONNECTED;
			game.getPlayer().setConnected(false);
			skh.getInSocket().close();
			skh.getOutSocket().close();
			try {
				skh.getSocket().close();
			} catch (IOException e) {}
		}
	}
	
	/**
	 * Receive the message from the client
	 * @param skh
	 * @return a string with the message
	 */
	private synchronized String receiveMessage(SocketHandler skh){
		String str = null;
		try{
			str = skh.getInSocket().next();
		} catch ( NoSuchElementException ex ){
			this.status = State.DISCONNECTED;
			game.getPlayer().setConnected(false);
			skh.getInSocket().close();
			skh.getOutSocket().close();
			try {
				skh.getSocket().close();
			} catch (IOException e) {}
		} catch ( IllegalStateException e ){
			this.status = State.DISCONNECTED;
			game.getPlayer().setConnected(false);
		} catch ( NullPointerException e ){
			this.status = State.DISCONNECTED;
			game.getPlayer().setConnected(false);
		}
		return str;
	}

	/**
	 * Method that change the state of the game and send messages to the clients
	 * @param msg, the message to send to th clients
	 * @param status, the state of the Game
	 */
	private synchronized void howToReadString(String msg, State status){
		
		// For a different state, we expect a different message
		switch (status){
		
			// Check if the player chooses a valid map
			case CHOOSE_MAP:
				if( msg.equalsIgnoreCase("fermi") || msg.equalsIgnoreCase("galilei") ||
						msg.equalsIgnoreCase("galvani"))
					this.map = msg;
				else if( msg.equals("Timeout!") ){
					this.status = State.AFK;
				}
				else{
					this.status = State.ERROR;
				}
				break;
				
			// Check if the player wants to see the map at the beginnig of his turn
			case SEE_MAP:
				if(msg.equals("y")){
					this.status = State.CHOOSE_MAP;
				}
				else if(msg.equals("n")){
					this.status = State.NEW_TURN;
				}
				else if(msg.equals("~")){
					game.getPlayer().setAfk();
					this.status = State.AFK;
				}
				else
					this.status = State.ERROR;
				break;
				
			// Check if the move of the player is valid
			case NEW_TURN:
				if( msg != null ){
					String[] coords = msg.split("&");
					char x;
					if(coords[0].equals(""))
						x = 'z';
					else
						x = coords[0].charAt(0);
					int y = Integer.parseInt(coords[1]);
					if(x == '~' || y == -7){
						game.getPlayer().setAfk();
						this.status = State.AFK;
					}
					else try {
						this.sec = this.game.move(x, y);
						} catch (IOException e) { /* do something */ }
					if( sec == null)
						this.status = State.ERROR;
				}
				break;
				
			// Check if the alien player wants to attack
			case ATTACK:
				if(msg.equals("y")){
					game.attack();
				}
				else if(msg.equals("n")){
					this.status = State.UPDATE;
				}
				else if(msg.equals("~")){
					game.getPlayer().setAfk();
					this.status = State.AFK;
				}
				else
					this.status = State.ERROR;
				break;
				
			// Check if the player wants to make noise in a valid sector
			case NOISE_ANY_SECTOR:
				if(msg != null){
					String[] coords = msg.split("&");
					char x;
					if(coords[0].equals(""))
						x = 'z';
					else
						x = coords[0].charAt(0);
					int y = Integer.parseInt(coords[1]);
					if(x == '~' || y == -7){
						game.getPlayer().setAfk();
						this.status = State.AFK;
					}
					if(game.checkSector(x, y)){
						this.noiseSec = new Sector(x, y);
					}
					else{
						this.status = State.ERROR;
					}
				}
				break;
			
			// If the player is disconnected, do nothing
			case DISCONNECTED:
				break;
		
		// It should never be here
		default:
			this.status = State.ERROR;
			break;	
		}
		
	}
	
	/**
	 * @return the status
	 */
	public State getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(State status) {
		this.status = status;
	}

	@Override
	public synchronized void run() {
		// Starting the game
		this.status = State.START_GAME;
		for(SocketHandler skh : sh){
			sendMessage(skh, status, String.valueOf(sh.size()));
		}
		
		// Choosing the map
		int id = 0;
		do{
			this.status = State.CHOOSE_MAP;
			for(SocketHandler skh : sh){
				if(skh.getPlayer().getId() != id)
					sendMessage(skh, status, null);
			}
			for(SocketHandler skh : sh){
				if(skh.getPlayer().getId() == id){
					sendMessage(skh, status, "choose");
					howToReadString(receiveMessage(skh), status);
					if(this.status == State.ERROR)
						sendMessage(skh, status, "wrong_map");
					if(this.status == State.AFK){
						sendMessage(skh, status, "you_afk");
						for(SocketHandler skhh : sh){
							sendMessage(skhh, status, "afk&"+skh.getPlayer().getId());
						}
						id++;
						if( id == sh.size() - 1 ){
							this.status = State.END_CONNECTION;
							update(this.status);
						}
						break;
					}
				}
			}
		
		} while (this.status == State.ERROR || this.status == State.AFK );
				
		// Telling everyone the playing map
		for(SocketHandler skh : sh)
			sendMessage(skh, status, map);
		
		// Creating the game
		if( map != null ) {
			this.game = new Game( this.sh, map );
		
		// Set afk the player which were suspende during the map choose
		if(id>0)
			for(int i = 0; i< id; i++){
				for(Player play : game.getPlayers()){
					if(play.getId() == i)
						play.setAfk();
					}
			}
				
			
		// Tell everyone their role
		this.status = State.YOUR_ROLE;
		for(SocketHandler skh : sh){
			for(Player play : game.getPlayers()){
				if(play.getSocket().equals(skh.getSocket()))
					sendMessage(skh, status, play.toString() + 
							"&" + play.getSector().toString());
			}
		}
		
		do{
			// Starting a new turn
			this.status = State.NEW_TURN;
			int turn = game.getCountTurn()/game.getNumPlayer();
			game.turnCounter();
			for(SocketHandler skh : sh){
				for(Player play : game.getPlayers()){
					if(play.getSocket().equals(skh.getSocket())){
						if (play.isConnected()){
							sendMessage(skh, status, "turn&"+(turn+1));
						}
					}
				}
			}
			
			// The players wait for their turn 
			for(SocketHandler skh : sh){
				for(Player play : game.getPlayers()){
					if(play.getSocket().equals(skh.getSocket())){
						if (!play.isHisTurn() && play.isConnected()){
							sendMessage(skh, status, "wait&"+game.getPlayer().getId());
						}
					}
				}
			}
			
			// It's player turn
			for(SocketHandler skh : sh){
				for(Player play : game.getPlayers()){
					if(play.getSocket().equals(skh.getSocket())){
						System.out.println(play.isAfk());
						if (play.isHisTurn() && !play.isAfk() && play.isConnected() && play.alive()){
							
							// The player choses if he wants to see the map
							do{
								this.status = State.SEE_MAP;
								sendMessage(skh, status, "map");
								howToReadString(receiveMessage(skh), status);
								if(this.status == State.CHOOSE_MAP)
									sendMessage(skh, status, map);
								if(this.status == State.ERROR)
									sendMessage(skh, status, "map");
							}while( this.status == State.ERROR );
							if( this.status == State.AFK )
								sendMessage(skh, status, "you_afk");
							
							// The player moves
							if(this.status != State.AFK){
								do{
									this.status = State.NEW_TURN;
									String nSector = wherePlayerCanMove();
									sendMessage(skh, status, "your_turn" + nSector);
									howToReadString(receiveMessage(skh), status);
									if(this.status == State.ERROR){
										sendMessage(skh, status, "invalid_move");
									}
								}while( this.status == State.ERROR );
								if( this.status == State.AFK )
									sendMessage(skh, status, "you_afk");
								else{
									if(sec != null){
										this.status = State.UPDATE;
										sendMessage(skh, status, "moved&" + this.sec.toString());
									
										// If a human player reaches the escape hatch sector,
										// he wins and the game ends
										if( sec instanceof EscapeHatchSector ){
											play.setVictory();
											this.status = State.END_GAME;
										}	
									
										// If the player is in a dangerous sector, the turn goes on
										else if( sec instanceof DangerousSector ){
										
											// An alien player can decide if he wants to attack
											if(play instanceof AlienPlayer){
												do{
													this.status = State.ATTACK;
													sendMessage(skh, status, "attack");
													howToReadString(receiveMessage(skh), status);
													if(this.status == State.ERROR)
														sendMessage(skh, status, "attack");
												}while( this.status == State.ERROR );
												if( this.status == State.AFK )
													sendMessage(skh, status, "you_afk");
											}
										
											// The player has to pick a card
											if(this.status == State.UPDATE){
												sendMessage(skh, status, "pick");
												Card card = game.pickSectorCard();
												if( card instanceof Silence ){
													this.status = State.SILENCE;
													sendMessage(skh, status, "null");
												}
												else if( card instanceof NoiseInYourSector ){
													this.status = State.NOISE_YOUR_SECTOR;
													sendMessage(skh, status, "null");
												}
												else {
													do{
														this.status = State.NOISE_ANY_SECTOR;
														sendMessage(skh, status, "null");
														howToReadString(receiveMessage(skh), status);
														if(this.status == State.ERROR)
															sendMessage(skh, status, "noise");
													} while(this.status == State.ERROR);
													if( this.status == State.AFK )
														sendMessage(skh, status, "you_afk");
												}
											}
										}
									}
								}
							}
						}
					}
				}		
			}
		
		// Updating all the player if the game is not already ended
		if(this.status != State.END_GAME)	
			update(this.status);
		
		// Checking if there is any condition that ends the game
		if(game.victoryCheck())
			this.status = State.END_GAME;
		
		// Updating all player if yhe game has ended, otherwise the game must go on
		if(this.status == State.END_GAME)
			update(this.status);
					
		} while(this.status != State.END_GAME);
		
		}
	}

	/**
	 * Method that enstablish where a player can move
	 * @return nsec, the list of sectors where the player can move
	 */
	private String wherePlayerCanMove() {
		String nSec = "";
		ArrayList<Sector> nSector = game.getPlayer().wherePlayerCanMove(game.getMap());
		for( Sector sec: nSector ){
			if(sec.getY()<10){
				nSec = nSec.concat("&" + sec.toString() + "." + sec.getX() + ".0" + sec.getY());
			}
			else{
				nSec = nSec.concat("&" + sec.toString() + "." + sec.getX() + "." + sec.getY());
			}
		}
		return nSec;
	}

	/**
	 * Method that give to the clients some information about the Game if the state changed
	 * @param state, the current state of the Game
	 */
	private synchronized void update(State state) {
		
		// Updating the player about the state of the game
		switch (state){
			
			// Closing the connection
			case END_CONNECTION:
				for(SocketHandler skh : sh){
					skh.getInSocket().close();
					skh.getOutSocket().close();
					try {
						skh.getSocket().close();
					} catch (IOException e) {}
				}
				break;
			
			// Updating the player about the end of the game
			case END_GAME:
				for(SocketHandler skh : sh){
					for(Player play : game.getPlayers()){
						if(play.getSocket().equals(skh.getSocket()) && play.isConnected()){
							if(play.isVictory())
								sendMessage(skh, status, "victory");
							else
								sendMessage(skh, status, "lose");
						}
					}
				}
				
				for(SocketHandler skh : sh){
					for(Player play : game.getPlayers()){
						if(play.getSocket().equals(skh.getSocket()) && play.isConnected()){
							skh.getInSocket().close();
							skh.getOutSocket().close();
							try {
								skh.getSocket().close();
							} catch (IOException e) {}
						}
					}
				}
				break;
				
			// Updating all the player that the turn of the playing user is over
			case UPDATE:
				for(SocketHandler skh : sh){
					for(Player play : game.getPlayers()){
						if(play.getSocket().equals(skh.getSocket()) && play.isConnected()){
							sendMessage(skh, status, "end_turn");
						}
					}
				}
				break;
				
			// Updating all player about the attack of an alien player
			case ATTACK:
				int count = 0;
				for(SocketHandler skh : sh){
					for(Player play : game.getPlayers()){
						if(play.getSocket().equals(skh.getSocket()) && play.isConnected()){
							sendMessage(skh, status, "attacked&" + sec.getX()+"&"+sec.getY()+"&"+game.getPlayer().getId()
								 );
							if(!play.alive()){
								sendMessage(skh, status, "dead");
								count ++;
							}
						}
					}
				}

				for(SocketHandler skh : sh){
					for(Player play : game.getPlayers()){
						if(play.getSocket().equals(skh.getSocket()) && play.isConnected()){
							sendMessage(skh, status, String.valueOf(count));
						}
					}
				}
				break;
			
			// Updating all players about silence in all sector
			case SILENCE:
				for(SocketHandler skh : sh){
					for(Player play : game.getPlayers()){
						if(play.getSocket().equals(skh.getSocket()) && play.isConnected()){
							sendMessage(skh, status, String.valueOf(game.getPlayer().getId()));
						}
					}
				}
				break;
				
			// Updating all player about where is the noise
			case NOISE_YOUR_SECTOR:
				for(SocketHandler skh : sh){
					for(Player play : game.getPlayers()){
						if(play.getSocket().equals(skh.getSocket()) && play.isConnected()){
							sendMessage(skh, status, sec.getX()+"&"+sec.getY()+"&"+game.getPlayer().getId());
						}
					}
				}
				break;
				
			// Updating all player about where the current player decded to make noise
			case NOISE_ANY_SECTOR:
				for(SocketHandler skh : sh){
					for(Player play : game.getPlayers()){
						if(play.getSocket().equals(skh.getSocket()) && play.isConnected()){
							sendMessage(skh, status, noiseSec.getX()+"&"+noiseSec.getY()+"&"+game.getPlayer().getId());
						}
					}
				}
				break;
				
			// Updating all player if some user has been suspended
			case AFK:
				int cnt = 0;
				for(SocketHandler skh : sh){
					for(Player play : game.getPlayers()){
						if(play.getSocket().equals(skh.getSocket()) && play.isConnected()){
							sendMessage(skh, status, "afk&"+game.getPlayer().getId());
							cnt++;
						}
					}
				}
				
				// If too many player has been suspended, the game ends
				if(cnt == game.getPlayers().size() || cnt == game.getPlayers().size()){
					for(SocketHandler skh : sh){
						for(Player play : game.getPlayers()){
							if(play.getSocket().equals(skh.getSocket()) && play.isConnected()){
								sendMessage(skh, status, "too_many");
							}
						}
					}
				this.status = State.END_GAME;
				}
				
				// If all the human player has been suspended, the game ends
				boolean end = true;
				for(Player play : game.getPlayers()){
					if(play instanceof HumanPlayer && !play.isAfk() && play.isConnected()){
						end = false;
						break;
					}
				if(end)
					this.status = State.END_GAME;
				}
				break;
				
			// Updating all players if someone has disconnected from the game
			case DISCONNECTED:
				cnt = 0;
				for(SocketHandler skh : sh){
					for(Player play : game.getPlayers()){
						if(play.getSocket().equals(skh.getSocket()) && play.isConnected()){
							sendMessage(skh, status, ""+game.getPlayer().getId());
						}
						else{
							cnt++;
						}
					}
				}
				
				// If too many player disconnected, the game ends
				if(cnt == game.getPlayers().size() || cnt == game.getPlayers().size()){
					for(SocketHandler skh : sh){
						for(Player play : game.getPlayers()){
							if(play.getSocket().equals(skh.getSocket()) && play.isConnected()){
								sendMessage(skh, status, "too_many");
							}
						}
					}
					this.status = State.END_GAME;
				}
				
				// If all the human player disconnected, the game ends
				end = true;
				for(Player play : game.getPlayers()){
					if(play instanceof HumanPlayer && play.isConnected()){
						end = false;
						break;
					}
				if(end)
					this.status = State.END_GAME;
				}
				break;
			
			// It should never be here
			default:
				break;
		}	
	}
}