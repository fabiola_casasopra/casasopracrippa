package servercontroller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.NotBoundException;
//import java.rmi.registry.LocateRegistry;
//import java.rmi.registry.Registry;
//import java.rmi.server.UnicastRemoteObject;

import view.Logger;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Executable class which spawns the Socket server and the RMI registry
 */
public class Server {
	private static Logger logger = new Logger();
	//private static boolean endServer = false;
	
	public static void main(String[] args) throws IOException, NotBoundException {
		//Registry registry = null;
		//String name = "RemoteGame";
		
		// Starting RMI server
		/*try {
			RemoteGame game = Tris.getInstance();
			RemoteGame stub = (RemoteGame) UnicastRemoteObject.exportObject(game, 0);
			registry = LocateRegistry.createRegistry(2026);
			registry.bind(name, stub);
			
			System.out.println("ComputeEngine bound");
		} catch (Exception e) {
			System.err.println("ComputeEngine exception:");
			registry = null;
		}*/

		// Starting socket server
		SocketServer server = new SocketServer();
		logger.log("Starting the server...");
		Thread t = new Thread(server);
		t.start();
		logger.log("Server started. Status: " + server.getStatus() + ". Port: " + server.getPort());
		
		boolean finish = false;
		//Loop to query the server status/stop the server.
		while(!finish){
			String read = readLine("Press S to get the server status;\nPress Q to exit\n");
			if(read.equals("Q")){
				finish = true;
			}
			if(read.equals("S")){
				logger.log("Server status: "+server.getStatus());
			}
		}
		
		//Releasing the server resources.
		t.interrupt();
		try {
			server.endListening();
		} catch (InterruptedException e) {}
	}
	
	/**
	 * Helper method to read a line from the console when the 
	 * program is started in Eclipse
	 * @param format the formet string to be read
	 * @param args arguments for the format
	 * @return the read string
	 * @throws IOException if the program cannot access the stdin
	 */
	private static String readLine(String format, Object... args) throws IOException {
	    if (System.console() != null) {
	        return System.console().readLine(format, args);
	    }
	    logger.log(String.format(format, args));
	    
	    BufferedReader br = null;
	    InputStreamReader isr = null;
	    String read = null;
	    
	    isr = new InputStreamReader(System.in);
	    br = new BufferedReader(isr);
	    read = br.readLine();
	    
	    return read;
	}
}