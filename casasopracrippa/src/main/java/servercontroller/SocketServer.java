package servercontroller;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

import view.Logger;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that implements a SocketServer.
 * The server accepts incoming connection, instantiates and starts a SocketHandler for each connection.
 */
public class SocketServer implements Runnable{
	
	private int port;
	private String address;
	private ServerSocket server;
	private boolean listening;
	private String status; 
	protected List<SocketHandler> sockethandlers = new LinkedList<SocketHandler>();
	protected List<Thread> thread = new LinkedList<Thread>();
	private Logger logger = new Logger();
	
	private static final int TIMEOUT = 120;
	private static final int MAX_NUM_PLAYER = 8;
	private static final int MIN_NUM_PLAYER = 2;

	/**
	 * Default constructor.
	 * The address is set to localhost; the port is set to 8888
	 */
	public SocketServer() {
		setPort( 8888 );
		setAddress( "0.0.0.0" );
		listening = false;
		status = "Created";
		sockethandlers = new LinkedList<SocketHandler>();
	}

	/**
	 * Field constructor
	 * @param port on which the server must listen
	 * @param address the address at which the server is reachable
	 */
	public SocketServer( int port, String address ) {
		super();
		this.setPort( port );
		this.setAddress( address );
		listening = false;
		status = "Created";
		sockethandlers = new LinkedList<SocketHandler>();
	}

	/**
	 * Method that give the status of the SocketServer
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	
	/**
	 * Method that give the port of the SocketServer
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * Method that set the port of the SocketServer
	 * @param port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * Method that give the address of the SocketServer
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Method that set the address of the SocketServer
	 * @param address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Method that put the server in a Listening state, 
	 * preparing it to accept incoming connections.
	 * @throws IOException if the server cannot be instantiated
	 */
	public void startListening() throws IOException, InterruptedException {
		if(!listening) {
			
			//Creates a server socket, bound to the specified port
			server = new ServerSocket( port );
			logger.log("Listening...");
			status = "Listening...";
			listening = true;
			long startTimer = System.currentTimeMillis();

			while( listening ) {
				//Listening loop
				try{
					
					//Timeout conditions
					long timeout = (System.currentTimeMillis() - startTimer) / 1000;
					if(sockethandlers.size() < MIN_NUM_PLAYER && timeout >= TIMEOUT) {
						for(SocketHandler sh : sockethandlers)
							sh.close();
						startTimer = System.currentTimeMillis();
					}
					
					if(sockethandlers.size() == MAX_NUM_PLAYER || (sockethandlers.size() >= MIN_NUM_PLAYER && timeout >= TIMEOUT )){
						
						// Starting a new game
						Thread t = new Thread(new GameServer(sockethandlers));
						thread.add(t);
						t.start();
						
						// Resetting
						sockethandlers = new LinkedList<SocketHandler>();
						startTimer = System.currentTimeMillis();
					}
					
					//Listens for a connection to be made to this socket and accepts it
					//The method blocks for one second
					server.setSoTimeout(1*1000);
					Socket s = server.accept();
					
					//A connection has been accepted 
					SocketHandler sh = new SocketHandler(s);
					sh.setIdPlayer(sockethandlers.size());
					
					//A new SocketHandler is add to the list
					sockethandlers.add( sh );
					
					//Start the socket handler
					sh.start();
					
				} catch ( IOException ex ){ 
					//System.out.println( "No connection!" );
				}
			}
		}
	}

	/**
	 * Method that stops the server from listening.
	 * It closes all the open sockets
	 * @throws IOException if some socket cannot be closed.
	 */
	public void endListening() throws InterruptedException {
			if( listening ){
				listening = false;
				for(SocketHandler sh : sockethandlers )
					try {
						sh.close();
					} catch (IOException er) {
						logger.log( "An I/O error has occured when closing the SocketHandler." );
					}

					try {
						server.close();
					} catch (IOException e) {
						logger.log( "An I/O error has occured when closing the socket." );
					}
					status = "Closed.";
			}
	}
	
	@Override
	public void run() {
		try {
			logger.log("Incomincio ad ascoltare...");
			startListening();		
		} catch (IOException ex) {
			status = "Error: "+ex.getMessage();
		} catch (InterruptedException ex) {
			status = "Error: "+ex.getMessage();
		}
	}
}
