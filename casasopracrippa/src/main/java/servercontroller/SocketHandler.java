package servercontroller;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

import playermodel.Player;
import view.Logger;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that handle a single socket connection
 */
public class SocketHandler extends Thread {
	
	private static int numClient = 0;
	private int id = numClient++;
	private int idPlayer;
	
	private Socket socket;
	private Player player;
	private PrintWriter outSocket;
	private Scanner inSocket;
	private Logger logger = new Logger();
	
	/**
	 * Constructor, the socket passed by the server
	 * Create any time a new connection is created, then it starts
	 * @param socket on which the handler and the client can communicate
	 */
	public SocketHandler(Socket socket) {
		super();
		this.socket = socket;
		this.player = new Player(null);
		player.setSocket(socket);	
	}
	
	/**
	 * @return the socket
	 */
	public Socket getSocket() {
		return socket;
	}
	
	/**
	 * @return the inSocket
	 */
	public Scanner getInSocket() {
		return inSocket;
	}

	/**
	 * @return the outSocket
	 */
	public PrintWriter getOutSocket() {
		return outSocket;
	}

	@Override
	public void run() {
		try{
			//Get the input and output stream
			inSocket = new Scanner(player.getSocket().getInputStream());
			outSocket = new PrintWriter(socket.getOutputStream());
			logger.log("Client "+id+" connesso");
            
            } catch (IOException e){
			logger.log("An I/O error has occured.");
		}
	}
	
	/**
	 * @return the player
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * Method that cleans the resources used by the SocketHandler
	 * @throws IOException if something cannot be closed
	 */
	public void close() throws IOException {
		getInSocket().close();
		getOutSocket().close();
		player.getSocket().close();
	}

	/**
	 * @return the idPlayer
	 */
	public int getIdPlayer() {
		return idPlayer;
	}

	/**
	 * @param idPlayer the idPlayer to set
	 */
	public void setIdPlayer(int idPlayer) {
		this.idPlayer = idPlayer;
		player.setId( idPlayer );
	}

}
