package playermodel;

import java.util.List;
import java.util.Vector;

import sectormodel.AlienSector;
import sectormodel.HumanSector;
import servercontroller.SocketHandler;
import deckcardmodel.AlienCard;
import deckcardmodel.Card;
import deckcardmodel.CharacterCardDeck;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the list of the player that are playing in the Game
 */
@SuppressWarnings("serial")
public class PlayerList extends Vector<Player> {
	
	/**
	 * Method that create the Player List
	 * @param sockethandlers, list of sockets of any client
	 * @param cardDeck, the Deck of Character Card
	 * @param as, the position of the Alien Sector
	 * @param hs, the position of the Human Sector
	 */
	public PlayerList( List<SocketHandler> sockethandlers, CharacterCardDeck cardDeck, AlienSector as, HumanSector hs){
		for(int i=0; i<sockethandlers.size(); i++){
			this.add(sockethandlers.get(i).getPlayer());
		}
		playerSort( sockethandlers, cardDeck, as, hs );
	}
	
	/**
	 * Method that initialize the players to Alien or Human
	 * @param the list of players, the character deck (already shuffled), the alien sector and the human sector of the chosen game map  
	 */
	private void playerSort( List<SocketHandler> sockethandlers, CharacterCardDeck cardDeck,
			AlienSector as, HumanSector hs ) {
		for( int i=0; i<this.size(); i++ ){
			Card card = this.get(i).pickCard(cardDeck);
			if(  card instanceof AlienCard ){
				this.set( i, new AlienPlayer( as ));
				this.get(i).setId(i);
				this.get(i).setSocket(sockethandlers.get(i).getSocket());
			}
			else {
				this.set( i, new HumanPlayer( hs ));
				this.get(i).setId(i);
				this.get(i).setSocket(sockethandlers.get(i).getSocket());
			}
		}
	}
}