package playermodel;

import java.util.ArrayList;

import mapsmodel.GameMap;
import sectormodel.AlienSector;
import sectormodel.EscapeHatchSector;
import sectormodel.Sector;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the Alien Player
 */
public class AlienPlayer extends Player {

	/**
	 * Method that create the Alien Player
	 * @param sector, initial sector of the player
	 */
	public AlienPlayer(Sector sector) {
		super(sector);
		if (sector == null) {
			throw new IllegalArgumentException("A player must be in a valid sector!");
		}
	}

	/**
	 * Method that prints the Alien Player
	 */
	@Override
	public String toString() {
		return "AlienPlayer";
	}

	/**
	 * Method that allows the Alien Player to attack
	 * @param players, the list of the players that are playing in the current game
	 */
	public void attack( PlayerList players ) {
		for( Player play : players ){
			if( play.getSector().getX() == this.getSector().getX() && 
					play.getSector().getY() == this.getSector().getY() && play.getId() != this.getId() )
				play.alive = false;
		}
	}
	
	/**
	 * Method that check the sectors where player can move
	 * @return the list of sector where player can move 
	 */
	@Override
	public ArrayList<Sector> wherePlayerCanMove( GameMap map ){
		ArrayList<Sector> nSector = new ArrayList<Sector>();
		nSector = map.maximumDistanceTwoSector( this.getSector() );
		for( int i = 0; i < nSector.size(); i++ ){
			if( nSector.get(i) instanceof EscapeHatchSector || nSector.get(i) instanceof AlienSector){
				nSector.remove(i);
			}
		}
		return nSector;
	}
}


