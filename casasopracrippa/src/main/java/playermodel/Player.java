package playermodel;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

import mapsmodel.GameMap;
import sectormodel.Sector;
import deckcardmodel.Card;
import deckcardmodel.CharacterCardDeck;
import deckcardmodel.SectorCardDeck;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the object Player
 */
public class Player {

	/**
	 * The current field where the player is placed, null if nowhere
	 */
	protected Sector sector;
	/**
	 * The card picked by the player
	 */
	protected Card card;
	/**
	 * The player is alive, false if is dead
	 */
	protected boolean alive = true;
	/**
	 * False until the player win
	 */
	private boolean victory = false;
	/**
	 * True when it's the player's turn
	 */
	private boolean hisTurn = false;
	/**
	 * True if the player does not move in time
	 */
	private boolean afk = false;
	/**
	 * The socket the player is connected with
	 */
	private Socket socket;
	/**
	 * True if the player is still connected
	 */
	private boolean connected = true;
	/**
	 * Player ID
	 */
	private int id;
	
	/**
	 * Constructor for the Player class
	 * @param sector where the player is placed, null for a generic player
	 */
	public Player(Sector sector) {
		this.sector = sector;
	}

	/**
	 * Method that checks if a player can move to a certain position
	 * @param finalSector, the target field where the player wants to move
	 * @return True if the player can move according to the rules of the game, false otherwise
	 */
	public boolean validMove(Sector finalSector, ArrayList<Sector> nSector) {
		for( Sector sec : nSector )
			if( sec.getX() == finalSector.getX() && sec.getY() == finalSector.getY() )
				return true;
		return false;
	}

	/**
	 * @return the sector
	 */
	public Sector getSector() {
		return sector;
	}

	/**
	 * Method that clone the player
	 */
	@Override
	public Player clone() {
		return this;		
	}

	/**
	 * Method that consent the player to pick a card
	 * @param cardDeck, the CharacterCard Deck from which the player pick a card
	 * @return the card picked by the player
	 */
	public Card pickCard(CharacterCardDeck cardDeck) {
		card = cardDeck.get(0);
		cardDeck.remove(0);
		return card;
	}
	
	/**
	 * Method that consent the player to pick a card
	 * @param cardDeck, the SectorCard Deck from which the player pick a card
	 * @return the card picked by the player
	 */
	public Card pickCard(SectorCardDeck cardDeck) {
		card = cardDeck.get(0);
		cardDeck.remove(0);
		return card;
	}
	
	/**
	 * Method that consent the player to pick a card
	 * @param cardDeck, the Deck from which the player pick a card
	 * @return the card picked by the player
	 */
	public Card pickCard(ArrayList<Card> deck) {
		card = deck.get(0);
		deck.remove(0);
		return card;
	}
	
	/**
	 * @return the player status, alive or dead
	 */
	public boolean alive() {
		return alive;
	}

	/**
	 * @return true if the player win
	 */
	public boolean isVictory() {
		return victory;
	}

	/**
	 * The player win
	 */
	public void setVictory() {
		this.victory = true;
	}
	
	/**
	 * @return true if it's player's turn 
	 */
	public boolean isHisTurn() {
		return hisTurn;
	}
	
	/**
	 * It's player's turn 
	 */
	public void setHisTurn() {
		this.hisTurn = true;
	}
	
	/**
	 * The player ends its turn
	 */
	public void endTurn() {
		this.hisTurn = false;
	}

	/**
	 * @return the afk state
	 */
	public boolean isAfk() {
		return afk;
	}

	/**
	 * The player is afk
	 * Once he is afk, he can't come back in the game
	 */
	public void setAfk() {
		this.afk = true;
	}

	/**
	 * The player must reveal his position
	 * @return the sector of the player
	 */
	public Sector revealToAll() {
		return sector;
	}

	/**
	 * The player's turn end
	 * @return the state of player's turn
	 */
	public boolean endPlay() {
		this.hisTurn = false;
		return isHisTurn();
	}

	/**
	 * Method that prints the player
	 */
	@Override
	public String toString() {
		return "Player";
	}
	
	/**
	 * Method that find sectors where the player can move
	 */
	public ArrayList<Sector> wherePlayerCanMove( GameMap map ){
		return null;
	}

	/**
	 * Method taht set plater's sector
	 * @param sec, the sector to be set
	 */
	public void setSector(Sector sec) {
		this.sector = sec;
	}
	
	/**
	 * Method that check if the move is valid
	 * @param map, the map on which the players are playing
	 * @param x, the first coordiante of the move
	 * @param y, the second coordiante of the move
	 * @return the valid sector where the player can move, null if the move is not valid
	 * @throws IOException
	 */
	public Sector setMove( GameMap map, char x, int y ) throws IOException{
		boolean valid = true;
	
		ArrayList<Sector> nSector = this.wherePlayerCanMove(map);
		if( x == '~' ){
			this.setAfk();
			return null;
		}

		if( y == -7 ){
			this.setAfk();
			return null;
		}
		if( x == 'z' || y == -1 )
			valid = false;
		if( valid ){
			for( Sector sec : nSector )
				if( sec.getX() == x && sec.getY() == y ){
					return sec;
				}
		}
			
		return null;
		
	}

	/**
	 * @return the socket
	 */
	public Socket getSocket() {
		return socket;
	}

	/**
	 * @param socket, the socket to set
	 */
	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	/**
	 * @return the connected
	 */
	public boolean isConnected() {
		return connected;
	}

	/**
	 * @param connected, the connected to set
	 */
	public void setConnected(boolean connected) {
		this.connected = connected;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id, the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
}