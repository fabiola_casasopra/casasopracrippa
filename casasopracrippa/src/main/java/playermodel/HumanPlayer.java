package playermodel;

import java.util.ArrayList;

import mapsmodel.GameMap;
import sectormodel.Sector;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the Human Player
 */
public class HumanPlayer extends Player {

	/**
	 * Method that create the Human Player
	 * @param sector, initial sector of the player
	 */
	public HumanPlayer(Sector sector) {
		super(sector);
		if (sector == null) {
			throw new IllegalArgumentException(
					"A player must be in a valid sector!");
		}
	}

	/**
	 * Method that prints the Human Player
	 */
	@Override
	public String toString() {
		return "HumanPlayer";
	}
	
	/**
	 * Method that check the sectors where player can move
	 * @return the list of sector where player can move 
	 */
	@Override
	public ArrayList<Sector> wherePlayerCanMove( GameMap map ){
		
		ArrayList<Sector> nSector = new ArrayList<Sector>();
		nSector = map.nearSector( this.getSector() );
		return nSector;
	}
}
