package commoncontroller;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Enumeration of all the states of the Game
 */
public enum State {
	START_GAME, CHOOSE_MAP, YOUR_ROLE, NEW_TURN, ATTACK, UPDATE, END_GAME, ERROR, 
	NOISE_ANY_SECTOR, SILENCE, NOISE_YOUR_SECTOR, AFK, DISCONNECTED, END_CONNECTION, SEE_MAP
}
