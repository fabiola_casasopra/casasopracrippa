package clientcontroller;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.util.InputMismatchException;
import java.util.Scanner;

import view.Logger;
import view.Print;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Method that implements the NetworkInterface.
 * It allows the client to communicate with the server over a socket connection.
 */
public class SocketClient implements NetworkInterface {
	
	private Socket s;
	private PrintWriter socketOut;
	private Scanner socketIn;
	private BufferedReader stdin;
	private Logger logger = new Logger();
	
	private static final int TIMEOUT = 60;
	
	/**
	 * Default constructor.
	 */
	public SocketClient() {
		
	}

	@Override
	public boolean connect() throws IOException {
		try {
			
			//Get the input stream to read from the stdin
			stdin = new BufferedReader( new InputStreamReader( System.in ) );
			
			//Create a socket and connect it to the server
			char answer = '~';
			do{
				logger.log("Do you want to use a local server?");
				answer = getAnswer();
			} while (answer != 'y' && answer != 'n' );
			if(answer == 'y')
				s = new Socket( "127.0.0.1", 8888 );
			else{
				logger.log("Insert the IP address of the server");
				String ip = "Timeout!";
				do{
					ip = getName();	
				} while(ip == "Timeout!");
				s = new Socket( ip, 8888 );
			}
		} catch ( UnknownHostException e ) {
			logger.log( "The IP address of the host could not be determined" );
			logger.log( "Not able to connect" );
			return false;
		} catch (IOException e) {
			logger.log( "An I/O exception has occurred" );
			logger.log( "Not able to connect" );
			return false;
		}
		
		try {
			//Get the output stream to write to the server
			socketOut = new PrintWriter(s.getOutputStream());
		} catch (IOException e) {			
			logger.log( "An I/O exception has occurred" );
			logger.log( "Closing the connection..." );
			s.close();
			return false;
		}
		
		try {
			//Get the input stream to read from the server
			socketIn = new Scanner(s.getInputStream());
		} catch (IOException e) {			
			logger.log( "An I/O exception has occurred" );
			logger.log( "Closing the connection..." );
			socketOut.close();
			s.close();
			return false;
		}
		
		logger.log( "Connecting..." );
		return true;
	}

	/**
	 * @return the PrintWriter
	 */
	public PrintWriter getPw() {
		return socketOut;
	}

	/**
	 * @return the Scanner
	 */
	public Scanner getBr() {
		return socketIn;
	}

	@Override
	public boolean close() throws IOException {
		try {
			socketOut.close();
			socketIn.close();
			s.close();
			return true;
		} catch(IOException ex) {
			logger.log( "An I/O exception has occurred" );
			logger.log( "Cannot close the connection (?)" );
			return false;
		}
	}

	@Override
	public String getMap() throws RemoteException {
		logger.log("You can choose between the following maps:");
		logger.log(" -> Galilei: perfect for new players: it is perfectly balanced between\n"
				+ "\tthe Humans and the Aliens.\n\tSuggested for 4 to 8 players.");
		logger.log(" -> Fermi: perfect for competitive players, as it is strategically demanding.\n"
				+ "\tNot recommended for novice players.\n\tSuggested for 2 to 6 players.");
		logger.log(" -> Galvani: good for experienced players, it seems simple at first, "
				+ "\n\tbut your early movements in the game will have a long-term impact on the outcome."
				+ "\n\tSuggested for 2 to 8 players.");
		logger.log("Which map do you want to use to play?");
		return getName();
	}
	
	@Override
	public void printMap(String map) {
		logger.log("Chosen map : " + map);
		try {
			(new Print()).printMap(map);
		} catch (IOException e) {
			// do something
		}
	}
	
	@Override
	public String seeMap() throws RemoteException {
		logger.log("Do you want to see the map? (y/n)");
		try {
			return Character.toString(getAnswer());
		} catch (IOException e) { 
			// do something 
		}
		return null;
	}

	@Override
	public String move() throws RemoteException {
		logger.log("Where do you want to move?");
		try {
			char x = getX();
			int y = getY();
			logger.log("x = " + x + " & y = " + y);
			return x+"&"+y;
		} catch (IOException e) {
			// do something
		}
		return null;
	}

	@Override
	public String attack() throws RemoteException {
		logger.log("Do you want to attack the sector you are in? (y/n)");
		logger.log("Warning: if you attack, you will reveal both your identity and position");
		try {
			return Character.toString(getAnswer());
		} catch (IOException e) { 
			// do something 
		}
		return null;
	}

	@Override
	public String noise() throws RemoteException {
		logger.log("In which sector do you want to make noise?");
		try {
			char x = getX();
			int y = getY();
			logger.log("x = " + x + " & y = " + y);
			return x+"&"+y;
		} catch (IOException e) {
			// do something
		}
		return null;
	}
	
	/**
	 * Method that asks the User to insert the name of the map for the current Game
	 * @return the name of the map
	 */
	private String getName() {
		long startTime = System.currentTimeMillis();
		try{
			while((System.currentTimeMillis() - startTime) < TIMEOUT * 1000 && !stdin.ready() ){}
			if( stdin.ready() ){ 
				return stdin.readLine();
			}
			else{
				logger.log( "Timeout!" );
				return "Timeout!";
			}
		} catch (NullPointerException e){
			return null;
		} catch (IOException ex){
			return null;
		}
	}

	/**
	 * Method that asks the User to insert the row of the sector in which he wants to move
	 * @return the row of the sector (the x coordinate)
	 * @throws IOException
	 */
	private char getX() throws IOException{
		logger.log("Insert the row of the sector you want to make in:");
		BufferedReader in = new BufferedReader( new InputStreamReader( System.in ) );
		long startTime = System.currentTimeMillis();
		try {
			while((System.currentTimeMillis() - startTime) < TIMEOUT * 1000 && !in.ready() ){}
		} catch (NullPointerException e) {
			return 'z';
		}
		if( in.ready() ) {
			String st=in.readLine();
			if(st.length() > 1)
				return 'z';
			else {
				char c = st.charAt(0);
				return c;
			}
		}
		else{
			logger.log( "Timeout!" );
			return '~';
		}
	}

	/**
	 * Method that asks the User to insert the column of the sector in which he wants to move
	 * @return the column of the sector (the y coordinate)
	 * @throws IOException
	 */
	private int getY() throws IOException{
		logger.log("Insert the column of the sector you want to make in:");
		BufferedReader in = new BufferedReader( new InputStreamReader( System.in ) );
		long startTime = System.currentTimeMillis();
		try {
			while((System.currentTimeMillis() - startTime) < TIMEOUT * 1000 && !in.ready() ){}
		} catch (NullPointerException e) {
			return -1;
		} catch (InputMismatchException ex){
			return -1;
		}

		if( in.ready() ){
			try{
			return Integer.parseInt(in.readLine());
			} catch (NumberFormatException nfe) {
				return -1;
			}
		}
		else{
			System.out.println( "Timeout!" );
			return -7;
		}
	}
	
	/**
	 * Method that get the answer of the User to the attack's and seeMap's questions
	 * @return the answer of the User or a value if the Timeout ends
	 * @throws IOException
	 */
	private char getAnswer() throws IOException{
		BufferedReader in = new BufferedReader( new InputStreamReader( System.in ) );
		long startTime = System.currentTimeMillis();
		try {
			while((System.currentTimeMillis() - startTime) < TIMEOUT * 1000 && !in.ready() ){}
		} catch (NullPointerException e) {
			return 'z';
		}
		if( in.ready() )
			return (char)in.read();
		else{
			logger.log( "Timeout!" );
			return '~';
		}
	}

}
