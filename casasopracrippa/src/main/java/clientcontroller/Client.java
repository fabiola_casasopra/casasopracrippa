package clientcontroller;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.NoSuchElementException;

import view.Logger;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class which is used to instantiate the client.
 * It asks to the user which network interface must be used.
 * This client starts a socket server and creates a RMI registry to be called by the server.
 */
public class Client {
	
	static boolean playing = true;
	private static Logger logger = new Logger();
	
	public static void main( String[] args ) throws IOException {
		
		/*//Which network interface must be used?
		String read = "";
		//while( !read.equals( "1" )  && !read.equals( "2" ) ){
		while( !read.equals( "1" ) ){
			System.out.println( "Choose your network interface:" );
			System.out.println( "1 - Socket" );
			//System.out.println( "2 - RMI" );
			read = readLine( "\n" );
			//if( !read.equals( "1" )  && !read.equals( "2" ) )
			if( !read.equals( "1" ) )
				System.out.println( "Wrong command!" );
		}
		
		//Factory method to create the interface
		NetworkInterface ni = NetworkInterfaceFactory.getInterface( read );*/
		
		SocketClient ni = new SocketClient();
		logger.log( "Connection with socket" );
		logger.log( "Connection to the game... Waiting for other player..." );
		//Using the socket client
		ni.connect();
		while(playing){
			//Receiving the message from the server
			String msg = receiveMessage(ni);
			if( msg != null){
				//Parsing the received message
				String[] params = msg.split("&");
				howToReadMsg(params, ni);
			}
		}
	}
	
	/**
	 * Method that allows the Client to comunicate with the User
	 * @param param, list of info used to determinate the current state of the Game
	 * @param ni, the Client Interface used
	 * @throws RemoteException
	 */
	private synchronized static void howToReadMsg(String[] param, SocketClient ni) throws RemoteException {
		String status = param[0];
		// Choosing what to do 
		switch(status){
			
			// Telling the user that the game has started and the number of player
			case "START_GAME":
				logger.log("Start game!");
				logger.log("The players number is " + param[1]);
			break;
			
			// It allows to make the user choosing the map or to print the map to the user
			case "CHOOSE_MAP":
				if(param[1].equalsIgnoreCase("fermi") || param[1].equalsIgnoreCase("galvani") ||
						param[1].equalsIgnoreCase("galilei"))
					ni.printMap(param[1]);
				else if(param[1].equals("choose"))
					sendMessage( ni, ni.getMap());
				else
					logger.log("Wait for the first player to chose the map");
				break;
			
			// Telling the user if he is an alien or a human player
			case "YOUR_ROLE":
				logger.log("\nYour role is " + param[1] + ", so you will start from the " + 
						param[2]);
				break;
			
			// Telling the user which turn it is or if it's his turn.
			// If so, it let him know where he can move, otherwise it tells him to wait
			case "NEW_TURN":
				if(param[1].equals("wait"))
					logger.log("\n\nIt's player " + param[2] + " turn. Wait...");
				else if(param[1].equals("your_turn")){
					logger.log("\n\nIt's your turn!");
					logger.log("Here you are the sector where you can move:");
					for(int i=2; i<param.length; i++)
						logger.log(param[i]);
					sendMessage( ni, ni.move());
				}
				else {
					logger.log("\n\nTurn " + param[2]);
				}
				break;
			
			// Asking the user if he wants to see the map again at the beginning of his turn
			case "SEE_MAP":
				sendMessage( ni, ni.seeMap());
				break;
			
			// Telling the player where it has moved or if he hsa picked a card
			case "UPDATE":
				if(param[1].equals("moved"))
					logger.log("You moved in a "+ param[2] );
				else if(param[1].equals("pick"))
					logger.log("You have to pick a card");
				break;
			
			// Asking the user if he wants to attack. If so, it tells to all users where he attacked and if there are some killed
			// Also telling you if you have been killed
			case "ATTACK":
				if(param[1].equals("attack")){
					sendMessage( ni, ni.attack());
				}
				else if(param[1].equals("dead"))
					logger.log("You have been killed!");
				else if(param[1].equals("attacked"))
					logger.log("Player "+ param[4] + ": I ATTACK IN SECTOR [" + param[2] + "," 
							+ param[3] + "]");
				else
					logger.log(param[1] + " killed");
				break;
				
			// Telling the user he picked a silence card and update all the other users about it
			case "SILENCE":
				if(param[1].equals("null"))
					logger.log("You picked a Silence card");
				else
					logger.log("Player "+ param[1] + ": SILENCE IN ALL SECTORS");
				break;
			
			// Telling the user he picked a noise in your sector card and update all the other users about it	
			case "NOISE_YOUR_SECTOR":
				if(param[1].equals("null"))
					logger.log("You picked a NoiseInYourSector card");
				else
					logger.log("Player "+ param[3] + ": NOISE IN SECTOR [" + param[1] + "," 
							+ param[2] + "]");
				break;
			
			// Telling the user he picked a noise in any sector card, asking him where he wants to make noise
			// and update all the other users about it
			case "NOISE_ANY_SECTOR":
				if(param[1].equals("null")){
					logger.log("You picked a NoiseInAnySector card");
					sendMessage( ni, ni.noise());
				}
				else
					logger.log("Player "+ param[3] + ": NOISE IN SECTOR [" + param[1] + "," 
							+ param[2] + "]");
				break;
			
			// Telling the user that the game is over and if he has won or lost
			case "END_GAME":
				logger.log("\n\nGAME OVER!\n\n");
				if(param[1].equals("victory"))
					logger.log("VICTORY :)");
				if(param[1].equals("lose"))
					logger.log("DEFEAT :(");
				break;
			
			// Telling the user that his input is invalid
			case "ERROR":
				if(param[1].equals("wrong_map"))
					logger.log("Choose a valid map!");
				else if(param[1].equals("map"))
					logger.log("Choose a valid command (y/n)!");
				else if(param[1].equals("invalid_move"))
					logger.log("Choose a valid move!");
				else if(param[1].equals("attack"))
					logger.log("Choose a valid command (y/n)!");
				else if(param[1].equals("noise"))
					logger.log("Choose a valid dangerous sector where to make noise!");
				else
					logger.log("Unknown error!");
				break;
			
			// Telling the user if he waited too long to insert the input and so he is suspended.
			// Also telling to all the users that he has been suspended
			case "AFK":
				if(param[1].equals("too_many"))
					logger.log("Too many player has been suspended. Ending game...");
				else if(param[1].equals("you_afk")){
					logger.log("You have waited too long to insert the input!");
					logger.log("So you have been suspended...");
				}
				else{
					logger.log("Player "+ param[2] + " has been suspended");
				}
				break;
			
			// Telling all the user that someone has disconnected from the game
			case "DISCONNECTED":
				logger.log("Player " + param[1] + " disconnected");
		}
	}
	
	/**
	 * Send the message to the client
	 * @param ni
	 * @param msgToClient
	 * @param info
	 */
	private static void sendMessage(SocketClient ni, String msgToServer){
		ni.getPw().println(msgToServer);
		ni.getPw().flush();
	}
	
	/**
	 * Receive the message from the client
	 * @param skh
	 * @return a string with the message
	 * @throws IOException
	 */
	private static String receiveMessage(SocketClient ni){
		String str = null;
		try{
			str = ni.getBr().next();				
		} catch (NoSuchElementException ex){
			try {
				ni.close();
				logger.err("\nNO CONNECTION\n");
				playing = false;
			} catch (IOException e) {}
		} catch (NullPointerException ez){
			logger.err("SERVER DOWN :(");
			playing = false;
		}
		return str;
	}
}
