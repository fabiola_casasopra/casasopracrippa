package clientcontroller;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Factory method class. It creates the correct Network Interface.
 */
public class NetworkInterfaceFactory {
	
	private NetworkInterfaceFactory() {
		
	}
	
	 /**
	  * Static method that creates the method interface
	  * @param net 1 to create a Socket interface, 2 to create an RMI interface
	  * @return the required NetworkInterface
	  */
	public static NetworkInterface getInterface( String net ) {
		if( net.equals("1") ) 
			return new SocketClient();
		else 
			return new RMIClient();
	}
}
