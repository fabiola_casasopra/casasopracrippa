package clientcontroller;

import java.io.IOException;
import java.rmi.RemoteException;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Interface tht exposes all the methods that must be implemented by all the available network interfaces
 */
public interface NetworkInterface {
	
	/**
	 * Method that connects the client to the server
	 * @return true if the connection is established successfully, false otherwise.
	 * @throws IOException
	 */
	boolean connect() throws IOException;
	/**
	 * Method that closes the connection
	 * @return true if the connection is closed successfully, false otherwise.
	 * @throws IOException
	 */
	boolean close() throws IOException;
	
	/**
	 * Method that get the name of the map
	 * @return the name of the map for the current game
	 * @throws RemoteException
	 */
	String getMap() throws RemoteException;
	
	/**
	 * Method that asks the player where he wants to move
	 * @return the player's move
	 * @throws RemoteException
	 */
	String move() throws RemoteException;
	
	/**
	 * Method that asks the alien player if he wants to attack
	 * @return the player's answer
	 * @throws RemoteException
	 */
	String attack() throws RemoteException;
	
	/**
	 * Method that asks the player in which sector he wants to make noise
	 * if the player has picked a noise in any sector card
	 * @return the sector chhosen by the player
	 * @throws RemoteException
	 */
	String noise() throws RemoteException;
	
	/**
	 * Method that prints the map
	 * @param map, the map to be printed
	 */
	void printMap(String map);
	
	/**
	 * Method that allows a player to see the map if he wants
	 * @return the name of the map
	 * @throws RemoteException
	 */
	String seeMap() throws RemoteException;
	
}

