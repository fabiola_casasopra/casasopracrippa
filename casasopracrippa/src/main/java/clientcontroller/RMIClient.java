package clientcontroller;

import java.io.IOException;
import java.rmi.RemoteException;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that implements the NetworkInterface.
 * It implements all the methods such that they can communicate with the server throught the RMI protocol.
 */
public class RMIClient implements NetworkInterface {

	@Override
	public boolean connect() throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean close() throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getMap() throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String move() throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String attack() throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String noise() throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void printMap(String map) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String seeMap() throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

}
