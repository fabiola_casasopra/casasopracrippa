package view;

import java.io.PrintStream;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent a Logger used to print Stings
 */
public class Logger {
	
	PrintStream out=System.out;
	PrintStream err=System.err;
	
	/**
	 * Method that print a Srting
	 * @param str, the String to be print
	 */
	public void log(String str) {
		out.println(str);
	}
	
	/**
	 * Method that print a Srting
	 * @param str, the String to be print
	 */
	public void print(String str) {
		out.print(str);
	}
	
	/**
	 * Method that print an Error Srting
	 * @param str, the String to be print
	 */
	public void err(String str) {
		err.println(str);
	}
}
