package view;

import sectormodel.*;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the object PrintDangerousSector
 */
public class PrintDangerousSector extends PrintSector {
	
	private Logger logger = new Logger();
	
	/**
	 * Method that print the dangerous sector
	 * @param sec, the sector to be printed
	 */
	@Override
	public void printSector( Sector sec ) {
		if( sec.getY() < 10 )
			logger.print( "/  D." + sec.getX() + "0" + sec.getY() + "  \\" );
		else
			logger.print( "/  D." + sec.getX() + sec.getY() + "  \\" );
	}
}
