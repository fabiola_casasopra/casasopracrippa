package view;

import sectormodel.Sector;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Abstract Class that represent the object PrintSector
 */
public  abstract class PrintSector {
	
	/**
	 * Method that print the sector
	 * @param sec, the sector to be printed
	 */
	public abstract void printSector( Sector sec );
}
