package view;

import sectormodel.*;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the object PrintFactory
 */
public class PrintFactory {
	
	/**
	 * Factory method used to get object of Print 
	 * @param sec, the sector to print
	 * @return the object Print used to print the sector
	 */
	public PrintSector getSector( Sector sec ){
		if( sec.toString() == "AlienSector" )
			return new PrintAlienSector();
		else if( sec.toString() == "DangerousSector" )
			return new PrintDangerousSector();
		else if( sec.toString() == "EscapeHatchSector" )
			return new PrintEscapeHatchSector();
		else if( sec.toString() == "HumanSector")
			return new PrintHumanSector();
		else if(sec.toString() == "SecureSector")
			return new PrintSecureSector();
		else
			return null;
	}
}