package view;

import sectormodel.Sector;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the object PrintHumanSector
 */
public class PrintHumanSector extends PrintSector {
	
	private Logger logger = new Logger();
	
	/**
	 * Method that print the human sector
	 * @param sec, the sector to be printed
	 */
	@Override
	public void printSector( Sector sec ) {
		if( sec.getY() < 10 )
			logger.print( "/  H." + sec.getX() + "0" + sec.getY() + "  \\" );
		else
			logger.print( "/  H." + sec.getX() + sec.getY() + "  \\" );
	}

}