package view;

import java.io.IOException;

import sectormodel.*;
import mapsmodel.*;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the object Print
 */
public class Print {
	
	protected static final int ROW = 14;
	protected static final int COL = 23;
	private Logger logger = new Logger();

	/**
	 * Method that print the map (CLI)
	 * @throws IOException 
	 */
	public void printMap( String gameMap ) throws IOException{
		
		GameMap map;
		 
		map=(new MapFactory()).getMap(gameMap);

		for (int j = 0; j < COL; j+=2) {
			logger.print( "  _______         " );
		}
		
		logger.log( "" );
		
		for(int i = 0; i < ROW; i++){
			
			//Even columns
			for (int j = 0; j < COL; j+=2){
				logger.print( " /       \\        " );
			}
			logger.log( "" );
			for (int j = 0; j < COL; j+=2){
				Sector sec = map.getTotalMap().get( (i*COL) + j ).get( 0 );
				if( sec == null)
					logger.print( "/         \\" );
				else
					(new PrintFactory()).getSector(sec).printSector(sec);

				if ( j != COL - 1)
					logger.print( "_______" );
			}
			logger.log( "" );
			
			//Odd columns
			logger.print( "\\        " );
			
			for (int j = 1; j < COL; j+=2){
				logger.print( " /       \\        " );
				if( j == COL - 2)
					logger.print(" /");
			}
			logger.log( "" );
			logger.print( " \\_______" );
			for (int j = 1; j < COL; j+=2){
				Sector sec = map.getTotalMap().get( (i*COL) + j ).get( 0 );
				if( sec == null)
					logger.print( "/         \\" );
				else
					(new PrintFactory()).getSector(sec).printSector(sec);
				
				logger.print( "_______" );
				
				if( j == COL - 2)
					logger.print("/");
			}
			logger.log( "" );
		}
		
		logger.print( "  " );
		
		for (int j = 1; j < COL; j+=2) {
			logger.print( "       \\         /" );
		}
			
		logger.log( "" );
		
		logger.print( " " );
		
		for (int j = 1; j < COL; j+=2) {
			logger.print( "         \\_______/" );
		}	
		
	}
}