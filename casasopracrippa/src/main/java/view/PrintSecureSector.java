package view;

import sectormodel.*;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the object PrintSecureSector
 */
public class PrintSecureSector extends PrintSector{
	
	private Logger logger = new Logger();

	/**
	 * Method that print the secure sector
	 * @param sec, the sector to be printed
	 */
	@Override
	public void printSector( Sector sec ) {
		if( sec.getY() < 10 )
			logger.print( "/  S." + sec.getX() + "0" + sec.getY() + "  \\" );
		else
			logger.print( "/  S." + sec.getX() + sec.getY() + "  \\" );
	}
}
