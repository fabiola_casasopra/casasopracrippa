package view;

import sectormodel.*;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the object PrintEscapeHatchSector
 */
public class PrintEscapeHatchSector extends PrintSector {
	
	private Logger logger = new Logger();

	/**
	 * Method that print the escape hatch sector
	 * @param sec, the sector to be printed
	 */
	@Override
	public void printSector( Sector sec ) {
		if( sec.getY() < 10 )
			logger.print( "/ EH." + sec.getX() + "0" + sec.getY() + "  \\" );
		else
			logger.print( "/ EH." + sec.getX() + sec.getY() + "  \\" );
	}
}