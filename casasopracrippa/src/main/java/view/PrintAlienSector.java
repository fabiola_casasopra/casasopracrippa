package view;

import sectormodel.*;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the object PrintAlienSector
 */
public class PrintAlienSector extends PrintSector {
	
	private Logger logger = new Logger();
	
	/**
	 * Method that print the alien sector
	 * @param sec, the sector to be printed
	 */
	@Override
	public void printSector( Sector sec ) {
		if( sec.getY() < 10 )
			logger.print( "/  A." + sec.getX() + "0" + sec.getY() + "  \\" );
		else
			logger.print( "/  A." + sec.getX() + sec.getY() + "  \\" );
	}
}