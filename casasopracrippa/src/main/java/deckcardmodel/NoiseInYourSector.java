package deckcardmodel;

/**
 *
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the Noise Card type that consent to make noise in the player's sector
 */
public class NoiseInYourSector extends Card {

	/**
	 * Method that print the NoiseInYourSector Card
	 */
	@Override
	public String toString() {
		return "NoiseInYourSectorCard";
	}
}
