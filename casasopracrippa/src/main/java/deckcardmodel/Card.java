package deckcardmodel;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Abstract Class that represent the object Card
 */
public abstract class Card {

	/**
	 * Method that print the Card
	 */
	@Override
	public abstract String toString();
}
