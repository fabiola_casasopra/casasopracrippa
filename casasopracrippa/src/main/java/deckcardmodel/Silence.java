package deckcardmodel;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the Noise Card type that consent to not make any noise
 */
public class Silence extends Card {

	/**
	 * Method that print the Silence Card
	 */	
	@Override
	public String toString() {
		return "SilenceCard";
	}
}
