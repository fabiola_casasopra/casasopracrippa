package deckcardmodel;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the Noise Card type that consent to make noise in a not define sector
 */
public class NoiseInAnySector extends Card {

	/**
	 * Method taht print the NoiseInAnySector Card
	 */
	@Override
	public String toString() {
		return "NoiseInAnySectorCard";
	}
}
