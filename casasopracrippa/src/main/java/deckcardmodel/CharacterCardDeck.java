package deckcardmodel;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the Deck of Card composed by Character Card
 */
@SuppressWarnings("serial")
public class CharacterCardDeck extends Deck {
	
	/**
	 * Method that create a new CharacterCard deck
	 */
	public CharacterCardDeck( int numPlayer ){
		buildDeck( numPlayer );
		shuffleDeck();
	}

	/**
	 * Method that initialize the CharacterCard Deck
	 */
	@Override
	public void buildDeck(int numPlayer) {

		int dispari = 0;

		if ((numPlayer % 2) == 1)
			dispari = 1;

		for (int i = 0; i < numPlayer; i++) {
			if (i < (numPlayer + dispari) / 2) {
				this.add(new AlienCard());
			} else {
				this.add(new HumanCard());
			}
		}
	}

	/**
	 * Method that shuffle the CharacterCard Deck
	 */
	@Override
	public void shuffleDeck() {

		for (int i = 0; i < this.size(); i++) {
			int index = (int) (Math.random() * this.size());
			Card temp = this.get(i);
			this.set(i, this.get(index));
			this.set(index, temp);
		}
	}
}
