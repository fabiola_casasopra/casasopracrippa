package deckcardmodel;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the Human Card type
 */
public class HumanCard extends Card {

	/**
	 * Method that print the Human Card
	 */
	@Override
	public String toString() {
		return "HumanCard";
	}
}
