package deckcardmodel;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the Alien Card type
 */
public class AlienCard extends Card {
	
	/**
	 * Method that print the Alien Card
	 */
	@Override
	public String toString() {
		return "AlienCard";
	}
}