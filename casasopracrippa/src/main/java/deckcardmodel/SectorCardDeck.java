package deckcardmodel;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the Deck of Card composed by Sector Card
 */
@SuppressWarnings("serial")
public class SectorCardDeck extends Deck {
	
	/**
	 * The number of Card that compose the SectorCard Deck
	 */
	private final int SECTOR_CARDS = 25;
	
	/**
	 * Method that create a new SectorCard deck
	 */
	public SectorCardDeck(){
		int numCard = SECTOR_CARDS;
		buildDeck( numCard );
		shuffleDeck();
	}

	/**
	 * Method that initialize the SectorCard Deck
	 */
	@Override
	public void buildDeck(int numCard) {

		for (int i = 0; i < numCard; i++) {
			if (i % 5 == 0)
				this.add(new Silence());
			else if (i % 2 == 0)
				this.add(new NoiseInYourSector());
			else
				this.add(new NoiseInAnySector());
		}
	}

	/**
	 * Method that shuffle the SectorCard Deck
	 */
	@Override
	public void shuffleDeck() {

		for (int i = 0; i < this.size(); i++) {
			int index = (int) (Math.random() * this.size());
			Card temp = this.get(i);
			this.set(i, this.get(index));
			this.set(index, temp);
		}
	}
}
