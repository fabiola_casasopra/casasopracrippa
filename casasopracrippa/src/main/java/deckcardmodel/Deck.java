package deckcardmodel;

import java.util.ArrayList;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Abstract Class that represent the Deck of Card
 */
@SuppressWarnings("serial")
public abstract class Deck extends ArrayList<Card> {
	
	/**
	 * Method that create the deck
	 */
	public Deck(){}

	/**
	 * Method that shuffle the deck of cards, deck composed by a number of cards equals to the number of player
	 */
	public abstract void shuffleDeck();

	/**
	 * Method that initialize the Deck
	 * @param numPlayer, the number of players for the current game
	 */
	public abstract void buildDeck(int numPlayer);

	/**
	 * Method that confront two Decks
	 * @return true if the two Deck are equal.
	 */
	public boolean equalsTo( ArrayList<Card> deck ) {
		for( int i=0; i<deck.size(); i++  )
			if( !deck.get(i).toString().equals(this.get(i).toString()))
					return false;
		return true;
	}
	
	/**
	 * Method that confront two Decks
	 * @return true if the two Deck have the same card number and the number of a certain type of card is the same in both the deck.
	 */
	public boolean similarTo(ArrayList<Card> deck) {
		
		int numAlienCard = 0, numHumanCard = 0;
		int numNoiseInYourSector = 0, numNoiseInAnySector = 0, numSilence = 0;
		if( ! (deck.size() == this.size()))
			return false;
		if( deck.get(0) instanceof AlienCard || deck.get(0) instanceof HumanCard ){
			for( int i=0; i<deck.size(); i++ ){
				if( deck.get(i) instanceof AlienCard )
					numAlienCard++;
				else if( deck.get(i) instanceof HumanCard )
					numHumanCard++;
			}
			if( (numAlienCard + numHumanCard) != deck.size() )
				return false;
		}		
		else{
			for( int i=0; i<deck.size(); i++ ){
				if( deck.get(i) instanceof NoiseInYourSector )
					numNoiseInYourSector++;
				else if( deck.get(i) instanceof NoiseInAnySector )
					numNoiseInAnySector++;
				else if( deck.get(i) instanceof Silence )
					numSilence++;
			}
			if( (numNoiseInYourSector + numNoiseInAnySector + numSilence) != deck.size() )
				return false;
		}
		return true;
	}

}