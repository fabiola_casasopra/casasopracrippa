package gamemodel;

import java.io.IOException;
import java.util.List;

import deckcardmodel.*;
import mapsmodel.*;
import playermodel.*;
import sectormodel.*;
import servercontroller.SocketHandler;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the object Game
 */
public class Game {
	
	// variables to end the game
	private int countTurn = 0;
	private int numPlayer;
	private final int MAX_TURNS = 39;
	private List<SocketHandler> sh;
	
	// model of the game
	private CharacterCardDeck ccd;
	private SectorCardDeck scd;
	private int gameTurn;
	private PlayerList players;
	private Player player;
	private GameMap map;
	
	/**
	 * Method that create the Game
	 * @param sockethandlers, the list of sockets associated to each client
	 * @param mapToBeCreated, the name of the map for the current Game
	 */
	public Game(List<SocketHandler> sockethandlers, String mapToBeCreated ){
		this.sh = sockethandlers;
		try {
			setMap(mapToBeCreated);
		} catch (IOException e) {
			// do something
		}
		this.numPlayer = sh.size(); 
		this.ccd = new CharacterCardDeck( numPlayer );
		this.scd = new SectorCardDeck();
		this.gameTurn = sh.size() * MAX_TURNS;
		AlienSector as = (AlienSector) getMap().findAlienSector();
		HumanSector hs = (HumanSector) getMap().findHumanSector();
		setPlayers(new PlayerList( sockethandlers, ccd, as, hs ));
	}

	/**
	 * Method that count the turns
	 */
	public void turnCounter() {
		int k  = this.countTurn % this.numPlayer;
		for(Player play : this.getPlayers())
			if(play.getId() == k){
				play.setHisTurn();
				this.setPlayer(play);
			}
			else
				play.endTurn();
		this.countTurn++;
	}
	
	/**
	 * Method that set the Map for the current Game
	 * @param newMap, the name if the MAp for the current Game
	 * @throws IOException
	 */
	public void setMap(String newMap) throws IOException{
		MapFactory mf = new MapFactory();
		this.map = mf.getMap(newMap);
	}
	
	/**
	 * Method that moves the player
	 * @param map, the map used to play
	 * @param x, the first coordinate of the sector in which the player wants to move
	 * @param y, the second coordinate of the sector in which the player wants to move
	 * @throws IOException
	 * @return the sector in which the player moved, null if the sector is invalid
	 */
	public Sector move(char x, int y) throws IOException{
		Sector sec = getPlayer().setMove(this.getMap(), x, y);
		if( sec == null )
			return null;
		getPlayer().setSector(sec);
		return sec;
	}
	
	/**
	 * Method that allows the Alien Player to attack
	 */
	public void attack(){
		if( this.getPlayer() instanceof AlienPlayer)
			((AlienPlayer) this.getPlayer()).attack(getPlayers());
	}
	
	/**
	 * Method that allows the player to pick a sector card
	 * @return the picked card
	 */
	public Card pickSectorCard(){
		Card card = getPlayer().pickCard(scd);
		if(scd.size() == 0)
			this.scd = new SectorCardDeck();
		return card;
	}
	
	/**
	 * Method that check if a sector is inside the current Game Map
	 * @param x, the first coordinate of the sector
	 * @param y, the second coordinate of the sector
	 * @return true if the sector is valid
	 */
	public boolean checkSector(char x, int y){
		return !getMap().checkSector(x, y);
	}

	/**
	 * Method that check if there are still humans alive
	 * @return false if there are no more human alive
	 */
	public boolean humanCheck() {
		for( Player play : this.getPlayers() )
			if( play instanceof HumanPlayer && play.alive() )
				return true;
		return false;
	}
	
	/**
	* Method that check if there is any condition for the victory
	* @return true if someone has won
	*/
	public boolean victoryCheck() {
		if( !this.humanCheck() ){
			for( Player play : getPlayers() )
				if( play instanceof AlienPlayer && play.alive() && !play.isAfk() )
					play.setVictory();
			return true;
		}
		if( this.countTurn >= gameTurn ){
			for( Player play : getPlayers() )
				if( play instanceof AlienPlayer && play.alive() && !play.isAfk() )
					play.setVictory();
			return true;
		}
		return false;
	}
	
	/**
	 * Method that check if the current Game is end
	 * @return true if the game is end
	 */
	public boolean endGame() {
		if( this.victoryCheck() )
			return true;
		return false;
	}
	
	/**
	 * @return the number of player
	 */
	public int getNumPlayer() {
		return numPlayer;
	}

	/**
	 * @return the number of the turn
	 */
	public int getCountTurn() {
		return countTurn;
	}

	/**
	 * @return the list of Players for the current game
	 */
	public PlayerList getPlayers() {
		return players;
	}

	/**
	 * @param players, the list of players to be set
	 */
	public void setPlayers(PlayerList players) {
		this.players = players;
	}

	/**
	 * @return the player which is playing during the current turn
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * @param player, the player to be set for the current turn
	 */
	public void setPlayer(Player player) {
		this.player = player;
	}

	/**
	 * @return the map of the current game
	 */
	public GameMap getMap() {
		return map;
	}
}