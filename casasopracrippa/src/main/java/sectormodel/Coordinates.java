package sectormodel;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the object Coordinates
 */
public class Coordinates {
	/**
	 * The x coordinate
	 */
	private char x;
	/**
	 * The y coordinate
	 */
	private int y;

	/**
	 * Constructor for the Coordinates class
	 * @param x coordinate
	 * @param y coordinate
	 */
	public Coordinates(char x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * @return the x
	 */
	public char getX() {
		return x;
	}

	/**
	 * @param x, the x to set
	 */
	public void setX(char x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param y, the y to set
	 */
	public void setY(int y) {
		this.y = y;
	}
}
