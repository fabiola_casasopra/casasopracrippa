package sectormodel;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the Human Sector
 */
public class HumanSector extends Sector {
	/**
	 * Constructor for the HumanSector class
	 * @param x coordinate
	 * @param y y coordinate
	 */
	public HumanSector(char x, int y) {
		super(x, y);
	}

	/**
	 * Method that prints the Human Sector
	 */
	@Override
	public String toString() {
		return "HumanSector";
	}
	
}
