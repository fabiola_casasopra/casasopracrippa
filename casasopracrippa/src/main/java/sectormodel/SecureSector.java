package sectormodel;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the Secure Sector
 *
 */
public class SecureSector extends Sector {
	/**
	 * Constructor for the SecureSector class
	 * @param x coordinate
	 * @param y coordinate
	 */
	public SecureSector(char x, int y) {
		super(x, y);
	}

	/**
	 * Method that prints the Secure Sector
	 */
	@Override
	public String toString() {
		return "SecureSector";
	}
}
