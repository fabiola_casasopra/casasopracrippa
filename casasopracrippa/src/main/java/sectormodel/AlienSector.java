package sectormodel;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the Alien Sector
 */
public class AlienSector extends Sector {
	/**
	 * Constructor for the AlienSector class
	 * @param x coordinate
	 * @param y coordinate
	 */
	public AlienSector(char x, int y) {
		super(x, y);
	}

	/**
	 * Method that prints the Alien Sector
	 */
	@Override
	public String toString() {
		return "AlienSector";
	}
	
}
