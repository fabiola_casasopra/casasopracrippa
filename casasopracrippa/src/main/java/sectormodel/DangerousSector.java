package sectormodel;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the Dangerous Sector
 */
public class DangerousSector extends Sector {
	/**
	 * Constructor for the DangerousSector class
	 * @param x coordinate
	 * @param y coordinate
	 */
	public DangerousSector(char x, int y) {
		super(x, y);
	}

	/**
	 * Method that prints the Dangerous Sector
	 */
	@Override
	public String toString() {
		return "DangerousSector";
	}	
}
