package sectormodel;

import playermodel.Player;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the EscapeHatch Sector
 */
public class EscapeHatchSector extends Sector {
	/**
	 * Constructor for the EscapeHatchSector class
	 * @param x coordinate
	 * @param y coordinate
	 */
	public EscapeHatchSector(char x, int y) {
		super(x, y);
	}

	/**
	 * Method that set victory for the human player
	 */
	public void humanVictory(Player player) {
		player.setVictory();
	}

	/**
	 * Method that prints the EscapeHatch Sector
	 */
	@Override
	public String toString() {
		return "EscapeHatchSector";
	}
}
