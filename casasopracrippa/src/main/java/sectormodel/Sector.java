package sectormodel;

import java.util.ArrayList;

/**
 * 
 * @author Mattia Crippa & Fabiola Casasopra
 * Class that represent the object Sector
 */
public class Sector extends Coordinates {

	/**
	 * Constructor for the Sector class
	 * @param x coordinate
	 * @param y y coordinate
	 */
	public Sector(char x, int y) {
		super(x, y);
	}
	
	/**
	 * Method that prevents the Alien Player to return to the starting sector in a move with a displacement of 2 sectors
	 * @param sectorList, the list of sector
	 * @return true if the sector is already inside the sector list, false otherwise
	 */
	public boolean isThere( ArrayList<Sector> sectorList ){
		for( Sector sec : sectorList ){
			if( sec.getX() == this.getX() && sec.getY() == this.getY() )
				return true;
		}
		return false;
	}
	
	/**
	 * Method that prints the sector
	 */
	@Override
	public String toString() {
		return "Sector";
	}

}
